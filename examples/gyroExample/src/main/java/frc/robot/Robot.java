package frc.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

public class Robot extends TimedRobot
{
  public static final SPI.Port GYRO_PORT = SPI.Port.kOnboardCS0;
  public static final int CONTROLLER_PORT = 0;
  public static final double DEAD_ZONE_VALUE = 0.1;

  public static final int RIGHT_BACK = 1;
  public static final int RIGHT_FRONT = 2;
  public static final int LEFT_FRONT = 3;
  public static final int LEFT_BACK = 4;

  WPI_TalonSRX leftFront;
  WPI_TalonSRX rightFront;
  WPI_TalonSRX leftBack;
  WPI_TalonSRX rightBack;
  SpeedControllerGroup leftGroup;
  SpeedControllerGroup rightGroup;

  DifferentialDrive driveTrain;

  XboxController xboxPad;
  ADXRS450_Gyro gyro;

  double kAngleSetpoint = 0.0;
  double kP = 0.005;

  @Override
  public void robotInit()
  {
    gyro = new ADXRS450_Gyro(GYRO_PORT);
    //gyro.reset();
    gyro.calibrate();

    if(driveTrain == null)
    {
      leftFront = new WPI_TalonSRX(LEFT_FRONT);
      rightFront = new WPI_TalonSRX(RIGHT_FRONT);
      leftBack = new WPI_TalonSRX(LEFT_BACK);
      rightBack = new WPI_TalonSRX(RIGHT_BACK);

      leftGroup = new SpeedControllerGroup(leftFront, leftBack);
      rightGroup = new SpeedControllerGroup(rightFront, rightBack);

      driveTrain = new DifferentialDrive(leftGroup, rightGroup);
    }

    if(xboxPad == null)
    {
      xboxPad = new XboxController(CONTROLLER_PORT);
    }
  }

  @Override
  public void teleopPeriodic()
  {
    double y = valueDeadZone(xboxPad.getRawAxis(1), DEAD_ZONE_VALUE);
    double turningValue = (kAngleSetpoint - gyro.getAngle()) * kP;
    turningValue = Math.copySign(turningValue, xboxPad.getY());
    driveTrain.arcadeDrive(y, turningValue);
    outputDebug(turningValue);
  }

  public void outputDebug(double turningValue)
  {
    System.out.println("WARNING! turningValue: " + turningValue);
    System.out.println("WARNING! gyro.getAngle(): " + gyro.getAngle());
  }

  public double valueDeadZone(double in, double deadValue)
  {
    if (Math.abs(in) < deadValue)
      return 0;
    else
      return in;
  }
}
