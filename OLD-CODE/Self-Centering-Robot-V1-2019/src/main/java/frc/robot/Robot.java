package frc.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.drive.MecanumDrive;


public class Robot extends IterativeRobot
{
  public static final boolean USING_AUTONOMOUS_ROTATION = true;
  public static final boolean USING_AUTONOMOUS_STRAFE = false;
  public static final boolean USING_CAMERA = true;
  public static final boolean USING_DEADZONE = true;
  public static final boolean RUNNING_LOCALLY = false;

  public static final int LEFT_FRONT = 3;
  public static final int RIGHT_FRONT = 2;
  public static final int LEFT_BACK = 4;
  public static final int RIGHT_BACK = 1;
  
  public static final int CONTROLLER_PORT = 0;
  public static final double DEAD_ZONE_VALUE = 0.1;

  int delay = 10;
  double autoSpeed = .15;

  XboxController xboxPad;
  UsbCamera visionCamera;

  WPI_TalonSRX leftFront;
  WPI_TalonSRX rightFront;
  WPI_TalonSRX leftBack;
  WPI_TalonSRX rightBack;

  MecanumDrive driveTrain;

  NetworkTableEntry visionEntry;
  NetworkTable table;
  NetworkTableInstance inst;

  double decision;

  public void visionDecider(double decision, double movementSpeed)
  {
    if(USING_AUTONOMOUS_ROTATION)
    {
        if(decision == -1)
        {
            System.out.println("WARNRING! Turning LEFT.");
            driveTrain.driveCartesian(0.0, 0.0, -movementSpeed);
        }
        if(decision == 1)
        {
            System.out.println("WARNRING! Turning RIGHT.");
            driveTrain.driveCartesian(0.0, 0.0, movementSpeed);
        }
        if(decision == 0)
        {
            System.out.println("WARNRING! CENTERED.");
            driveTrain.driveCartesian(0.0, 0.0, 0.0);
        }
    }
    
    if(USING_AUTONOMOUS_STRAFE)
    {
        if(decision == -1)
        {
            System.out.println("WARNRING! Strafing LEFT.");
            driveTrain.driveCartesian(-movementSpeed, 0.0, 0.0);
        }
        if(decision == 1)
        {
            System.out.println("WARNRING! Strafing RIGHT.");
            driveTrain.driveCartesian(movementSpeed, 0.0, 0.0);
        }
        if(decision == 0)
        {
            System.out.println("WARNRING! CENTERED.");
            driveTrain.driveCartesian(0.0, 0.0, 0.0);
        }
    }
  }
  @Override
  public void robotInit()
  {
    if(xboxPad == null)
    {
      xboxPad = new XboxController(CONTROLLER_PORT);
    }

    if(driveTrain == null)
    {
      leftFront = new WPI_TalonSRX(LEFT_FRONT);
      rightFront = new WPI_TalonSRX(RIGHT_FRONT);
      leftBack = new WPI_TalonSRX(LEFT_BACK);
      rightBack = new WPI_TalonSRX(RIGHT_BACK);

      driveTrain = new MecanumDrive(leftFront, leftBack, rightFront, rightBack);
    }

    if(RUNNING_LOCALLY)
    {
      inst = NetworkTableInstance.getDefault();
      table = inst.getTable("vision");
      visionEntry = table.getEntry("decision");
      inst.startClient("127.0.0.1");

      while (true)
      {
        try
        {
          Thread.sleep(delay);
        }
        catch (InterruptedException ex)
        {
          System.out.println("interrupted");
          return;
        }
        decision = visionEntry.getDouble(0.0);
        visionDecider(decision, autoSpeed);
      }
    }
    if(visionCamera == null && USING_CAMERA)
    {
      visionCamera = CameraServer.getInstance().startAutomaticCapture(0);
      System.out.println("WARNING! Camera Initializing!");
    }
  }

  public double valueDeadZone( double in, double deadValue )
  {
    if ( Math.abs( in ) < deadValue )
      return 0;
    else
      return in;
  }
  
  @Override
  public void teleopPeriodic()
  { 
    /*  Axis Information
    Axis 0: LJoyCon -> Left = -1, Right = 1
    Axis 1: LJoyCon -> Up = -1, Down = 1
    Axis 3: RJoyCon -> Left = -1, Right = 1
    Axis 4: RJoyCon -> Up = -1, Down = 1  */
    if (USING_DEADZONE)
    {
      double y = valueDeadZone(xboxPad.getRawAxis(1), DEAD_ZONE_VALUE);
      double x = valueDeadZone(xboxPad.getRawAxis(0), DEAD_ZONE_VALUE);
      double z = valueDeadZone(xboxPad.getRawAxis(4), DEAD_ZONE_VALUE);
      driveTrain.driveCartesian(-x, y, z);
    }
    else
    {
      double x = xboxPad.getRawAxis(0);
      double y = xboxPad.getRawAxis(1);
      double z = xboxPad.getRawAxis(3);
      driveTrain.driveCartesian(x, y, z);
    }
  }
  
  @Override
  public void autonomousInit()
  {
    inst = NetworkTableInstance.getDefault();
      table = inst.getTable("vision");
      visionEntry = table.getEntry("decision");
      inst.startClientTeam(3488);
  }

  @Override
  public void autonomousPeriodic()
  {
    try
    {
      Thread.sleep(delay);
    }
    catch (InterruptedException ex)
    {
      System.out.println("interrupted");
      return;
    }
    decision = visionEntry.getDouble(0.0);
    visionDecider(decision, autoSpeed);
  }

  @Override
  public void robotPeriodic(){}

  @Override
  public void testPeriodic(){}
}
