package frc.robot.subsystems;

import frc.robot.Constants;
import com.kauailabs.navx.frc.AHRS;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Drivetrain extends SubsystemBase
{
  WPI_TalonFX leftMaster;
  WPI_TalonFX rightMaster;
  WPI_TalonFX leftSlave;
  WPI_TalonFX rightSlave;

  AHRS navx;

  DifferentialDriveKinematics kinematics;
  DifferentialDriveOdometry odometry;

  SimpleMotorFeedforward feedforward;

  PIDController leftPIDController;
  PIDController rightPIDController;

  Pose2d pose;

  @Override
  public void periodic()
  {
    pose = odometry.update(getHeading(), getLeftSpeed(), getRightSpeed());
  }

  public Drivetrain()
  {
    leftMaster = new WPI_TalonFX(Constants.FRONT_LEFT);
    rightMaster = new WPI_TalonFX(Constants.BACK_RIGHT);
    leftSlave = new WPI_TalonFX(Constants.BACK_LEFT);
    rightSlave = new WPI_TalonFX(Constants.FRONT_RIGHT);

    leftMaster.setNeutralMode(NeutralMode.Brake);
    rightMaster.setNeutralMode(NeutralMode.Brake);
    leftSlave.setNeutralMode(NeutralMode.Brake);
    rightSlave.setNeutralMode(NeutralMode.Brake);

    navx = new AHRS(SPI.Port.kMXP);

    kinematics = new DifferentialDriveKinematics(Constants.kTrackWidthMeters);
    odometry = new DifferentialDriveOdometry(getHeading());

    feedforward = new SimpleMotorFeedforward(Constants.ks, Constants.kv, Constants.ka);

    leftPIDController = new PIDController(Constants.kP, Constants.kI, Constants.kD);
    rightPIDController = new PIDController(Constants.kP, Constants.kI, Constants.kD);

    pose = new Pose2d();

    leftMaster.setInverted(Constants.LEFT_INVERTED);
    leftMaster.setSensorPhase(Constants.LEFT_PHASE);
    rightMaster.setInverted(Constants.RIGHT_INVERTED);
    rightMaster.setSensorPhase(Constants.RIGHT_PHASE);

    navx.reset();
  }

  public Rotation2d getHeading()
  {
    return Rotation2d.fromDegrees((navx.getAngle()) * (Constants.GYRO_REVERSED ? -1.0 : 1.0));
  }

  public DifferentialDriveWheelSpeeds getSpeeds() // Need to change equation here to convert encoders to meters per second (EPR is 22016, calculate circumference)
  {
    return new DifferentialDriveWheelSpeeds(
        leftMaster.getSelectedSensorVelocity() / Constants.kGearRatio * 2 * Math.PI * Constants.kWheelRadiusMeters / 60,
        -rightMaster.getSelectedSensorVelocity() / Constants.kGearRatio * 2 * Math.PI * Constants.kWheelRadiusMeters / 60
    );
  }

  public double getLeftSpeed() // Fix equation
  {
    //return leftMaster.getSelectedSensorVelocity() / 2048 * 10 / Constants.kGearRatio * 2 * Math.PI * Constants.kWheelRadiusMeters;
    return leftMaster.getSelectedSensorVelocity() / Constants.kGearRatio * 2 * Math.PI * Constants.kWheelRadiusMeters / 60;
  }

  public double getRightSpeed() // Fix equation
  {
    return -rightMaster.getSelectedSensorVelocity() / Constants.kGearRatio * 2 * Math.PI * Constants.kWheelRadiusMeters / 60;  
  }

  public DifferentialDriveKinematics getKinematics() {return kinematics;}

  public Pose2d getPose() {return pose;}

  public SimpleMotorFeedforward getFeedforward() {return feedforward;}

  public PIDController getLeftPIDController() {return leftPIDController;}

  public PIDController getRightPIDController() {return rightPIDController;}

  public void setOutputVolts(double leftVolts, double rightVolts)
  {
    leftMaster.set(leftVolts / 12);
    rightMaster.set(-rightVolts / 12); // Needs to be negative to run properly
  }

  public void reset() {odometry.resetPosition(new Pose2d(), getHeading());}
}
