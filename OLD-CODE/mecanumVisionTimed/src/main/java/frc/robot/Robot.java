/*
Random Notes
  - Distance Formula: Somewhat works, the farther away you are, the more inches off it gets. Maybe recallibrate?
  - Code only for tank drive. Mecanum will NOT be used anymore.

TO-DO
[ ] Change all code from mecanum to tank
[ ] Be able to move robot while it is centering itself... But only forwards and backwards.
*/

package frc.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.TimedRobot;

public class Robot extends TimedRobot
{
  public static final boolean USING_CAMERA = false;
  public static final boolean USING_DEBUG = true;
  public static final boolean USING_LIMELIGHT = true;

  public static final int RIGHT_BACK = 1;
  public static final int RIGHT_FRONT = 2;
  public static final int LEFT_FRONT = 3;
  public static final int LEFT_BACK = 4;
   
  public static final int CONTROLLER_PORT = 0;
  public static final int CAMERA_PORT = 0;

  public static final double DEAD_ZONE_VALUE = 0.1;

  double xThresh = 2;
  double areaThresh = .2;
  double areaAddition = .8;
  double movementSpeed = -.3;
  double centeringSpeed = .2;
  int defaultState = 1; // 1 = Rotation, 2 = Strafing NOTE: ONLY CODE FOR TANK

  // Distance Calculation Variables (Similar triangles)
  double physicalDistance = 56.25; // Inches
  double physicalLength = 15.5; // Inches
  double pxLength = 72; // Res: 320x240... Lowers the overall precision of the measuring
  double pxDistance;
  double distance;

  double xSpeed;
  double ySpeed;
  double zSpeed;

  XboxController xboxPad;
  UsbCamera camera;

  WPI_TalonSRX leftFront;
  WPI_TalonSRX rightFront;
  WPI_TalonSRX leftBack;
  WPI_TalonSRX rightBack;

  MecanumDrive driveTrain;

  NetworkTable netTable;
  NetworkTableInstance netInst;

  NetworkTableEntry targetStateEntry;
  NetworkTableEntry targetXOffsetEntry;
  NetworkTableEntry targetYOffsetEntry;
  NetworkTableEntry targetAreaEntry;
  NetworkTableEntry targetHorizontalEntry;

  double tv;
  double tx;
  double ty;
  double ta;
  double thor;

  @Override
  public void robotInit()
  {
    if(xboxPad == null)
    {
      xboxPad = new XboxController(CONTROLLER_PORT);
    }

    if(driveTrain == null)
    {
      leftFront = new WPI_TalonSRX(LEFT_FRONT);
      rightFront = new WPI_TalonSRX(RIGHT_FRONT);
      leftBack = new WPI_TalonSRX(LEFT_BACK);
      rightBack = new WPI_TalonSRX(RIGHT_BACK);

      driveTrain = new MecanumDrive(leftFront, leftBack, rightFront, rightBack);
    }

    if(netTable == null && USING_LIMELIGHT)
    {
      netTable = NetworkTableInstance.getDefault().getTable("limelight");
      targetStateEntry = netTable.getEntry("tv");
      targetXOffsetEntry = netTable.getEntry("tx");
      targetYOffsetEntry = netTable.getEntry("ty");
      targetAreaEntry = netTable.getEntry("ta");
      targetHorizontalEntry = netTable.getEntry("thor");
    }

    if(camera == null && USING_CAMERA)
    {
      camera = CameraServer.getInstance().startAutomaticCapture(CAMERA_PORT);
      System.out.println("WARNING! Camera Initializing!");
    }
  }

  @Override
  public void teleopPeriodic()
  {
    double x = valueDeadZone(xboxPad.getRawAxis(0), DEAD_ZONE_VALUE);
    double y = valueDeadZone(xboxPad.getRawAxis(1), DEAD_ZONE_VALUE);
    double z = valueDeadZone(xboxPad.getRawAxis(4), DEAD_ZONE_VALUE);
    driveTrain.driveCartesian(-x, y, z);

    if(xboxPad.getBButton())
    {
      tx = targetXOffsetEntry.getDouble(0.0);
      positionRobot(tx, -1, xThresh, 0.0, 100, centeringSpeed, movementSpeed + y, 1);
    }

    if(xboxPad.getYButton())
    {
      thor = targetHorizontalEntry.getDouble(0.0);
      distance = targetDistance(thor, physicalDistance, physicalLength, pxLength);
      System.out.println("WARNING! Distance away from target: " + distance);
    }
  }

  @Override
  public void autonomousInit()
  {
    System.out.println("WARNING! Running Autonomous!");
  }

  @Override
  public void autonomousPeriodic()
  {
    tv = targetStateEntry.getDouble(0.0);
    tx = targetXOffsetEntry.getDouble(0.0);
    ty = targetYOffsetEntry.getDouble(0.0);
    ta = targetAreaEntry.getDouble(0.0);
    thor = targetHorizontalEntry.getDouble(0.0);
    distance = targetDistance(thor, physicalDistance, physicalLength, pxLength);
    positionRobot(tx, ta, xThresh, areaThresh, areaAddition, centeringSpeed, movementSpeed, defaultState);
    outputDebug();
  }

  public void positionRobot(double xPos, double area,
                            double xThresh, double areaThresh, double areaAddition,
                            double centeringSpeed, double movementSpeed, int state)
  {
    // Set centering speed
    if(xPos >= -xThresh && xPos <= xThresh)
    {
      xSpeed = zSpeed = 0.0;
    }
    else if(xPos <= -xThresh)
    {
      xSpeed = zSpeed = -centeringSpeed;
    }
    else if(xPos >= xThresh)
    {
      xSpeed = zSpeed = centeringSpeed;
    }

    // Set movement speed
    if(area >= areaThresh && area <= areaThresh + areaAddition)
    {
      ySpeed = 0.0;
    }
    else if(area <= areaThresh)
    {
      ySpeed = -movementSpeed;
    }
    else if(area >= areaThresh + areaAddition)
    {
      ySpeed = movementSpeed;
    }

    // Move robot accordingly
    if(state == 1)
    {
      driveTrain.driveCartesian(0.0, ySpeed, zSpeed);
    }
    else if(state == 2)
    {
      driveTrain.driveCartesian(xSpeed, ySpeed, 0.0);
    }
  }

  public void outputDebug()
  {
    if(USING_DEBUG)
    {
      System.out.println("WARNING! tv: " + tv);
      System.out.println("WARNING! tx: " + tx);
      System.out.println("WARNING! ty: " + ty);
      System.out.println("WARNING! ta: " + ta);
      System.out.println("WARNING! thor: " + thor);
      System.out.println("WARNING! distance: " + distance);
    }
  }

  public double valueDeadZone(double in, double deadValue)
  {
    if (Math.abs(in) < deadValue)
      return 0;
    else
      return in;
  }

  public double targetDistance(double thor, double physicalDistance, double physicalLength, double pxLength)
  {
    pxDistance = (physicalDistance * pxLength) / physicalLength;
    distance = (physicalLength * pxDistance) / thor;
    return distance;
  }

  @Override
  public void robotPeriodic(){}

  @Override
  public void testPeriodic(){}
}
