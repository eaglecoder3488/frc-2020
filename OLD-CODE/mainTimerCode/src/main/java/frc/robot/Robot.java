/*
Notes
  - Distance Formula: Somewhat works, the farther away you are, the more inches off it gets.

TO-DO
  [ ] Vision Code:
    [ ] Check if values need to be reversed. (getBButton())
    [ ] Check if limelight pipeline and led control is fuctional
    [ ] Recalibrate Distance Formula
  [ ] Fix gyro drift? (I don't think you can. Something about temperature.)
  [ ] Make sure encoders are working, check what data they output
  [ ] CONVERT CODE TO COMMAND BASED
  [ ] CALCULATING TRAJECTORY + PATHWEAVER
*/

package frc.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.TimedRobot;

public class Robot extends TimedRobot
{
  public static final boolean USING_DEBUG = true;
  public static final boolean USING_CAMERA = false;
  public static final boolean USING_LIMELIGHT = true;
  public static final boolean USING_GYRO = true;
  public static final boolean USING_SRX_ENCODERS = true;

  public static final int RIGHT_BACK = 3;
  public static final int RIGHT_FRONT = 4;
  public static final int LEFT_FRONT = 1;
  public static final int LEFT_BACK = 2;
   
  public static final int CONTROLLER_PORT = 0;
  public static final int CAMERA_PORT = 0;

  public static final double DEAD_ZONE_VALUE = 0.1;

  // Limelight Vision Variables
  double xThresh = 2;
  double areaThresh = .2;
  double areaAddition = .8;
  double movementSpeed = .3;
  double centeringSpeed = .2;

  // Distance Calculation Variables (Similar triangles)
  double physicalDistance = 56.25; // Inches
  double physicalLength = 15.5; // Inches
  double pxLength = 72; // Res: 320x240... Lowers the overall precision of the measuring
  double pxDistance;
  double distance;

  XboxController xboxPad;
  UsbCamera camera;
  AHRS ahrs;

  WPI_TalonSRX leftFront;
  WPI_TalonSRX rightFront;
  WPI_TalonSRX leftBack;
  WPI_TalonSRX rightBack;
  SpeedControllerGroup leftGroup;
  SpeedControllerGroup rightGroup;

  DifferentialDrive driveTrain;

  NetworkTable netTable;
  NetworkTableInstance netInst;

  NetworkTableEntry targetStateEntry;
  NetworkTableEntry targetXOffsetEntry;
  NetworkTableEntry targetYOffsetEntry;
  NetworkTableEntry targetAreaEntry;
  NetworkTableEntry targetHorizontalEntry;

  NetworkTableEntry pipelineEntry; 
  NetworkTableEntry ledModeEntry;

  double pipeline = 1;  // 0 - Default, 1 - Tape, 2 - Power Cell
  double defaultLedState = 1; // 0 - Pipeline, 1 - Off, 2 - Blink, 3 - On

  double tv;
  double tx;
  double ty;
  double ta;
  double thor;

  @Override
  public void robotInit()
  {
    if(xboxPad == null)
    {
      xboxPad = new XboxController(CONTROLLER_PORT);
    }

    if(driveTrain == null)
    {
      leftFront = new WPI_TalonSRX(LEFT_FRONT);
      rightFront = new WPI_TalonSRX(RIGHT_FRONT);
      leftBack = new WPI_TalonSRX(LEFT_BACK);
      rightBack = new WPI_TalonSRX(RIGHT_BACK);

      leftGroup = new SpeedControllerGroup(leftFront, leftBack);
      rightGroup = new SpeedControllerGroup(rightFront, rightBack);

      driveTrain = new DifferentialDrive(leftGroup, rightGroup);
    }

    if(netTable == null && USING_LIMELIGHT)
    {
      netTable = NetworkTableInstance.getDefault().getTable("limelight");
      targetStateEntry = netTable.getEntry("tv");
      targetXOffsetEntry = netTable.getEntry("tx");
      targetYOffsetEntry = netTable.getEntry("ty");
      targetAreaEntry = netTable.getEntry("ta");
      targetHorizontalEntry = netTable.getEntry("thor");
      pipelineEntry = netTable.getEntry("pipeline");
      ledModeEntry = netTable.getEntry("ledMode");
      setPipeline(pipeline);
    }

    if(camera == null && USING_CAMERA)
    {
      camera = CameraServer.getInstance().startAutomaticCapture(CAMERA_PORT);
      System.out.println("WARNING! Camera Initializing!");
    }

    if(ahrs == null && USING_GYRO)
    {
      ahrs = new AHRS(SPI.Port.kMXP);
      ahrs.reset();
    }
  }

  @Override
  public void teleopPeriodic()
  {
    double x = valueDeadZone(xboxPad.getRawAxis(0), DEAD_ZONE_VALUE);
    double y = valueDeadZone(xboxPad.getRawAxis(1), DEAD_ZONE_VALUE);
    driveTrain.arcadeDrive(-y, x);

    if(xboxPad.getBButton())
    {
      tx = targetXOffsetEntry.getDouble(0.0);
      positionRobot(tx, -1, xThresh, 0.0, 100, centeringSpeed, y);
    }

    if(xboxPad.getYButton())
    {
      thor = targetHorizontalEntry.getDouble(0.0);
      distance = targetDistance(thor, physicalDistance, physicalLength, pxLength);
      System.out.println("WARNING! Distance away from target: " + distance);
    }

    if(xboxPad.getAButton())
    {
      rightBack.setSelectedSensorPosition(0);
      leftFront.setSelectedSensorPosition(0);
    }

    if(USING_SRX_ENCODERS)
    {
      System.out.println("WARNING! rightFront: " + rightBack.getSelectedSensorPosition());
      System.out.println("WARNING! leftBack: " + leftFront.getSelectedSensorPosition());
    }

    if(USING_GYRO)
    {
      System.out.println("WARNING! gyro.getAngle(): " + ahrs.getAngle());
    }
  }

  @Override
  public void autonomousInit()
  {
    System.out.println("WARNING! Running Autonomous!");
  }

  @Override
  public void autonomousPeriodic()
  {
    tv = targetStateEntry.getDouble(0.0);
    tx = targetXOffsetEntry.getDouble(0.0);
    ty = targetYOffsetEntry.getDouble(0.0);
    ta = targetAreaEntry.getDouble(0.0);
    thor = targetHorizontalEntry.getDouble(0.0);
    distance = targetDistance(thor, physicalDistance, physicalLength, pxLength);
    positionRobot(tx, ta, xThresh, areaThresh, areaAddition, centeringSpeed, movementSpeed);
    outputDebug();
  }

  public void positionRobot(double xPos, double area,
                            double xThresh, double areaThresh, double areaAddition,
                            double centeringSpeed, double movementSpeed)
  {
    double xSpeed = 0.0;
    double ySpeed = 0.0;

    // Set rotation speed
    if(xPos >= -xThresh && xPos <= xThresh){}
    else if(xPos <= -xThresh) {xSpeed = -centeringSpeed;}
    else if(xPos >= xThresh)  {xSpeed = centeringSpeed;}

    // Set movement speed
    if(area >= areaThresh && area <= areaThresh + areaAddition){}
    else if(area <= areaThresh)                 {ySpeed = movementSpeed;}
    else if(area >= areaThresh + areaAddition)  {ySpeed = -movementSpeed;}

    driveTrain.arcadeDrive(xSpeed, ySpeed);
  }

  public void setPipeline(double pipeline)
  {
    pipelineEntry.setNumber(pipeline);
  }

  public void outputDebug()
  {
    if(USING_DEBUG)
    {
      System.out.println("WARNING! tv: " + tv);
      System.out.println("WARNING! tx: " + tx);
      System.out.println("WARNING! ty: " + ty);
      System.out.println("WARNING! ta: " + ta);
      System.out.println("WARNING! thor: " + thor);
      System.out.println("WARNING! distance: " + distance);
    }
  }

  public double valueDeadZone(double in, double deadValue)
  {
    if (Math.abs(in) < deadValue)
      return 0;
    else
      return in;
  }

  public double targetDistance(double thor, double physicalDistance, double physicalLength, double pxLength)
  {
    pxDistance = (physicalDistance * pxLength) / physicalLength;
    distance = (physicalLength * pxDistance) / thor;
    return distance;
  }

  @Override
  public void robotPeriodic(){}

  @Override
  public void testPeriodic(){}
}
