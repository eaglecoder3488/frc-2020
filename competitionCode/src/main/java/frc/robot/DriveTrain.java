package frc.robot;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import com.kauailabs.navx.frc.AHRS;

public class DriveTrain extends SubsystemBase
{
    // NOTE:  LEFT MOTORS - POSITIVE, RIGHT MOTORS - NEGATIVE
    //        WHEN DRIVING FORWARD

    private WPI_TalonFX frontLeftMotor;
    private WPI_TalonFX backLeftMotor;
    private WPI_TalonFX frontRightMotor;
    private WPI_TalonFX backRightMotor;

    private SpeedControllerGroup leftMotors;
    private SpeedControllerGroup rightMotors;

    private DifferentialDrive driveTrain;

    private DifferentialDriveKinematics kinematics;
    private DifferentialDriveOdometry odometry;
    private SimpleMotorFeedforward feedForward;
    
    private PIDController leftPIDController;
    private PIDController rightPIDController;

    private AHRS navx;

    public DriveTrain() 
    {
        frontLeftMotor = new WPI_TalonFX(Constants.FRONT_LEFT);
        backLeftMotor = new WPI_TalonFX(Constants.BACK_LEFT);
        frontRightMotor = new WPI_TalonFX(Constants.FRONT_RIGHT);
        backRightMotor = new WPI_TalonFX(Constants.BACK_RIGHT);

        frontLeftMotor.setNeutralMode(NeutralMode.Brake);
        backLeftMotor.setNeutralMode(NeutralMode.Brake);
        frontRightMotor.setNeutralMode(NeutralMode.Brake);
        backRightMotor.setNeutralMode(NeutralMode.Brake);

        leftMotors = new SpeedControllerGroup(frontLeftMotor, backLeftMotor);
        rightMotors = new SpeedControllerGroup(frontRightMotor, backRightMotor);

        driveTrain = new DifferentialDrive(leftMotors, rightMotors);

        navx = new AHRS(SPI.Port.kMXP);
        System.out.println(navx.getAngle());
        
        resetEncoders();
        
        kinematics = new DifferentialDriveKinematics(Constants.kTrackWidthMeters);
        odometry = new DifferentialDriveOdometry(getHeading());        
        feedForward = new SimpleMotorFeedforward(Constants.kS, Constants.kV, Constants.kA);
        leftPIDController = new PIDController(Constants.kP, Constants.kI, Constants.kD);
        rightPIDController = new PIDController(Constants.kP, Constants.kI, Constants.kD);
    }

    @Override
    public void periodic() {odometry.update(getHeading(), getLeftWheelDistance(), getRightWheelDistance());}

    public DifferentialDriveKinematics getKinematics() {return kinematics;}
    public DifferentialDriveOdometry getOdometry() {return odometry;}
    public SimpleMotorFeedforward getSimpleMotorFeedForward() {return feedForward;}
    public PIDController getLeftPIDCOntroller() {return leftPIDController;}
    public PIDController getRightPIDCOntroller() {return rightPIDController;}
    public Pose2d getPose() {return odometry.getPoseMeters();}
    public void setMaxOutput(double maxOutput) {driveTrain.setMaxOutput(maxOutput);}
    public void resetEncoders() {frontLeftMotor.setSelectedSensorPosition(0); frontRightMotor.setSelectedSensorPosition( 0 );}
    public int getLeftEncoderPosition() {return frontLeftMotor.getSelectedSensorPosition();}
    public int getRightEncoderPosition() {return -frontRightMotor.getSelectedSensorPosition();}
    public int getLeftEncoderVelocity() {return frontLeftMotor.getSelectedSensorVelocity();}
    public int getRightEncoderVelocity() {return -frontRightMotor.getSelectedSensorVelocity();}
    public void resetGyro() {navx.reset();}
    public double getGyroAngle() {return navx.getAngle();}
    public Rotation2d getHeading() {return Rotation2d.fromDegrees(navx.getAngle());}

    public void resetOdometry(Pose2d pose) 
    {
        resetEncoders();
        resetGyro();
        odometry.resetPosition(pose, getHeading());
    }

    public void tankDriveVolts(double leftVolts, double rightVolts)
    {
        System.out.println("WARNING! Position: " + getLeftWheelDistance() + " " + getRightWheelDistance());
        leftMotors.setVoltage(leftVolts);
        rightMotors.setVoltage(-rightVolts);
        driveTrain.feed();
    }
    
    public DifferentialDriveWheelSpeeds getSpeeds() 
    {
        // returns wheel speeds in meters per second
        double leftRotationsPerSecond = (double) getLeftEncoderVelocity() / Constants.sensorUnitsPerRevolution / Constants.kGearRatio * 10;
        double leftVelocity = leftRotationsPerSecond * 2 * Math.PI * Constants.kWheelRadiusMeters;
        double rightRotationsPerSecond = (double) getRightEncoderVelocity() / Constants.sensorUnitsPerRevolution / Constants.kGearRatio * 10;
        double rightVelocity = rightRotationsPerSecond * 2 * Math.PI * Constants.kWheelRadiusMeters;
        
        System.out.println("WARNING! Velocity: " + leftVelocity + " " + rightVelocity);

        return new DifferentialDriveWheelSpeeds(leftVelocity, rightVelocity);
    }

    // Distance returned in meters
    public double getLeftWheelDistance()
    {
        double leftDistance = ((double) getLeftEncoderPosition()) / Constants.sensorUnitsPerRevolution / Constants.kGearRatio * 2 * Math.PI * Constants.kWheelRadiusMeters;
        return leftDistance;
    }

    // Distance returned in meters
    public double getRightWheelDistance()
    {
        double rightDistance = ((double) getRightEncoderPosition()) / Constants.sensorUnitsPerRevolution / Constants.kGearRatio * 2 * Math.PI * Constants.kWheelRadiusMeters;
        return rightDistance;
    }
        
    public void printEncoders()
    {
        System.out.print("WARNING! LEFT Position: " + frontLeftMotor.getSelectedSensorPosition());
        System.out.print("WARNING! Velocity: " + frontLeftMotor.getSelectedSensorVelocity());
        System.out.println();
        System.out.print("WARNING! RIGHT Position: " + frontRightMotor.getSelectedSensorPosition());
        System.out.print("WARNING! Velocity: " + frontRightMotor.getSelectedSensorVelocity());
        System.out.println();
        System.out.println( "----------------------------------------");
    }
}