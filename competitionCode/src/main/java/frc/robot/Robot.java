/*
TO-DO LIST
  - Counter Code for shooter and Indexer
  - Limelight functionality
  - RGB for when indexer is full
    - 2 Thresholds: Normal Thresh, and the Warning thresh to say "Hey, I need to be shot!"
  - Trajectory + PathWeaver
*/

package frc.robot;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;
import com.playingwithfusion.TimeOfFlight;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;

public class Robot extends TimedRobot
{
  Command autonomousCommand;
  RobotContainer robotContainer;

  XboxController xboxPad;
  UsbCamera frontCamera;
  UsbCamera backCamera;
  AHRS navx;

  boolean controllerDebugMode;
  boolean shooterReady;
  boolean indexerFull;

  WPI_TalonFX leftMaster;
  WPI_TalonFX rightMaster;
  WPI_TalonFX leftSlave;
  WPI_TalonFX rightSlave;

  SpeedControllerGroup leftMotors;
  SpeedControllerGroup rightMotors;

  DifferentialDrive driveTrain;

  WPI_VictorSPX climberMaster;
  WPI_VictorSPX climberSlave;

  WPI_VictorSPX intakeMaster;

  WPI_VictorSPX conveyorBelt;
  WPI_TalonSRX indexer;

  WPI_TalonSRX shooterMaster;

  int shooterCounter;

  WPI_TalonSRX spinnerA;
  WPI_TalonSRX spinnerB;

  Spark rgbController;

  TimeOfFlight indexSensorIn;
  TimeOfFlight indexSensorOut;

  NetworkTable netTable;
  NetworkTableInstance netInst;

  NetworkTableEntry targetStateEntry;
  NetworkTableEntry targetXOffsetEntry;
  NetworkTableEntry targetYOffsetEntry;
  NetworkTableEntry targetAreaEntry;
  NetworkTableEntry targetHorizontalEntry;
  NetworkTableEntry pipelineEntry; 
  NetworkTableEntry ledModeEntry;

  @Override
  public void robotInit()
  {
    if(xboxPad == null) {xboxPad = new XboxController(Constants.CONTROLLER_PORT);}
    
    if(driveTrain == null)
    {
      leftMaster = new WPI_TalonFX(Constants.FRONT_LEFT);
      rightMaster = new WPI_TalonFX(Constants.BACK_RIGHT);
      leftSlave = new WPI_TalonFX(Constants.BACK_LEFT);
      rightSlave = new WPI_TalonFX(Constants.FRONT_RIGHT);

      setDriveSensorPhase(Constants.LEFT_PHASE, Constants.RIGHT_PHASE);
      setDriveMode(NeutralMode.Brake);

      leftMotors = new SpeedControllerGroup(leftMaster, leftSlave);
      rightMotors = new SpeedControllerGroup(rightMaster, rightSlave);

      leftMotors.setInverted(Constants.LEFT_INVERTED);
      rightMotors.setInverted(Constants.RIGHT_INVERTED);

      driveTrain = new DifferentialDrive(leftMotors, rightMotors);
    }

    if(climberMaster == null || climberSlave == null)
    {
      climberMaster = new WPI_VictorSPX(Constants.CLIMBER_MASTER);
      climberSlave = new WPI_VictorSPX(Constants.CLIMBER_SLAVE);
    }

    if(intakeMaster == null) {intakeMaster = new WPI_VictorSPX(Constants.INTAKE);}

    if(shooterMaster == null) {shooterMaster = new WPI_TalonSRX(Constants.SHOOTER);}

    if(conveyorBelt == null)
    {
      conveyorBelt = new WPI_VictorSPX(Constants.INDEX_IN);
      indexer = new WPI_TalonSRX(Constants.INDEX_OUT);
    }

    if(indexSensorIn == null && Constants.USING_INDEX_SENSORS)
    {
      indexSensorIn = new TimeOfFlight(Constants.INDEX_SENSOR_IN);
      indexSensorOut = new TimeOfFlight(Constants.INDEX_SENSOR_OUT);
      indexSensorIn.setRangingMode(Constants.sensorMode, Constants.sensorRate);
      indexSensorOut.setRangingMode(Constants.sensorMode, Constants.sensorRate);
    }

    if(spinnerA == null)
    {
      spinnerA = new WPI_TalonSRX(Constants.SPINNER_A);
      spinnerB = new WPI_TalonSRX(Constants.SPINNER_B);
    }

    if(navx == null && Constants.USING_GYRO)
    {
      navx = new AHRS(SPI.Port.kMXP);
      navx.reset();
    }

    if(rgbController == null && Constants.USING_RGB)
    {
      rgbController = new Spark(Constants.RGB_CONTROLLER);
      setRobotColor(Constants.robotColor);
    }

    if(robotContainer == null && Constants.USING_PATHWEAVER) {robotContainer = new RobotContainer();}

    if(netTable == null && Constants.USING_LIMELIGHT)
    {
      netTable = NetworkTableInstance.getDefault().getTable("limelight");
      targetStateEntry = netTable.getEntry("tv");
      targetXOffsetEntry = netTable.getEntry("tx");
      targetYOffsetEntry = netTable.getEntry("ty");
      targetAreaEntry = netTable.getEntry("ta");
      targetHorizontalEntry = netTable.getEntry("thor");
      pipelineEntry = netTable.getEntry("pipeline");
      ledModeEntry = netTable.getEntry("ledMode");
      setPipeline(Constants.pipeline);
      setLedState(1); // Keep the limelight leds off during initialization
    }

    if(frontCamera == null && Constants.USING_FRONT_CAMERA)
    {
      frontCamera = CameraServer.getInstance().startAutomaticCapture(Constants.FRONT_CAMERA_PORT);
      System.out.println("WARNING! Front Camera Initializing!");
    }

    if(backCamera == null && Constants.USING_BACK_CAMERA)
    {
      backCamera = CameraServer.getInstance().startAutomaticCapture(Constants.BACK_CAMERA_PORT);
      System.out.println("WARNING! Back Camera Initializing!");
    }
  }

  @Override
  public void teleopInit()
  {
    if(autonomousCommand != null && Constants.USING_PATHWEAVER) {autonomousCommand.cancel();}
    controllerDebugMode = false;
    setRobotColor(Constants.robotColor);
    shooterCounter = 0;
    indexerFull = false;
  }

  @Override
  public void teleopPeriodic()
  {
    double x = valueDeadZone(xboxPad.getRawAxis(Constants.xAxis), Constants.DEAD_ZONE_VALUE);
    double y = valueDeadZone(xboxPad.getRawAxis(Constants.yAxis), Constants.DEAD_ZONE_VALUE);
    driveTrain.arcadeDrive(y, -x);

    // Debug mode
    if(xboxPad.getBumper(Hand.kLeft))
    {
      setRobotColor(Constants.debugColor);
      // Inverted shooter
      if(xboxPad.getPOV() == Constants.dPadDown) {shooterMaster.set(-Constants.shooterSpeed);}
      else {shooterMaster.set(0);}

      // Inverted indexer
      if(xboxPad.getPOV() == Constants.dPadLeft) {indexer.set(-Constants.indexerSpeed);}
      else if(xboxPad.getPOV() == Constants.dPadRight) {indexer.set(Constants.indexerSpeed);}
      else {indexer.set(0);}

      // Normal climber (also just in case)
      if(xboxPad.getBumper(Hand.kRight)) {moveClimber();}
      else {stopClimber();}

      // Normal shooter
      if(xboxPad.getXButton()) {spinShooter();}
      else {stopShooter();}
    }

    // Normal Controls
    else
    {
      // Intake Mechanism
      if(xboxPad.getTriggerAxis(Hand.kRight) >= Constants.DEAD_ZONE_VALUE && !indexerFull)
      {
        intakeBall();
        moveConveyorBelt();
        System.out.println("WARNING! " + xboxPad.getTriggerAxis(Hand.kRight));
      }
      else
      {
        stopIntake();
        stopConveyorBelt();
      }

      // Inverted intake
      if(xboxPad.getAButton())
      {
        intakeMaster.set(-Constants.intakeSpeed);
        conveyorBelt.set(-Constants.conveyorBeltSpeed);
      }
      else if(xboxPad.getTriggerAxis(Hand.kRight) <= Constants.DEAD_ZONE_VALUE)
      {
        intakeMaster.set(0);
        conveyorBelt.set(0);
      }
      
      // Limelight centering
      if(xboxPad.getYButton())
      {
        double tx = targetXOffsetEntry.getDouble(0.0);
        double ta = targetAreaEntry.getDouble(0.0);
        positionRobot(tx, ta, Constants.xThresh, Constants.areaThresh, Constants.areaAddition, Constants.centeringSpeed, Constants.movementSpeed);
      }
      else {setLedState(1);}

      // Climber Mechanism
      if(xboxPad.getBumper(Hand.kRight)) {moveClimber();}
      else {stopClimber();}

      // Shooter Mechanism

      // Set warning color if the ball is close
      if(getIndexSensorOut() >= Constants.indexCapacity || getIndexSensorOut() <= 0) 
      {
        setRobotColor(Constants.robotColor);
      }
      else 
      {
        setRobotColor(Constants.warningColor);
        indexer.set(-.9);
        System.out.println( "BACK UP PLEASE" );
      }


      // Normal button press
      if(xboxPad.getXButton())
      {
        
        spinShooter();
        warmupCounter();
        if(shooterCounter >= Constants.warmupTime)
        {
          moveIndexer();
          if(shooterCounter >= Constants.warmupTime + Constants.indexerTime)
          {
            stopIndexer();
            shooterCounter = 0;
          }
        }
          
      }
      else if(getIndexSensorIn() >= Constants.sensorInThresh)
      {
        stopIndexer();
        stopShooter();
        shooterCounter = 0;
      }

      // Indexer Mechanism w/sensors
      if(Constants.USING_INDEX_SENSORS)
      {
        if(getIndexSensorIn() <= Constants.sensorInThresh && !indexerFull) {moveIndexer();}
        else {stopIndexer();}

        if(getIndexSensorOut() <= Constants.indexCapacity) {indexerFull = true;}
        else {indexerFull = false;}
      }

      // Indexer Mechanism w/out sensors
      /*if(!Constants.USING_INDEX_SENSORS)
      {
        if(xboxPad.getBButton()) {moveIndexer();}
        else {stopIndexer();}
      }
      */
    }
  }

  @Override
  public void autonomousInit()
  {
    autonomousCommand = robotContainer.getAutonomousCommand();
    if(Constants.USING_PATHWEAVER && autonomousCommand != null) {autonomousCommand.schedule();}
  }

  @Override
  public void autonomousPeriodic() {if(autonomousCommand != null && Constants.USING_PATHWEAVER) {CommandScheduler.getInstance().run();}}

  @Override
  public void testPeriodic()
  {
    if(Constants.USING_LIMELIGHT)
    {
      double tv = targetStateEntry.getDouble(0.0);
      double tx = targetXOffsetEntry.getDouble(0.0);
      double ty = targetYOffsetEntry.getDouble(0.0);
      double ta = targetAreaEntry.getDouble(0.0);
      double thor = targetHorizontalEntry.getDouble(0.0);
      double distance = getDistance();
      positionRobot(tx, ta, Constants.xThresh, Constants.areaThresh, Constants.areaAddition, Constants.centeringSpeed, Constants.movementSpeed);
      outputDebug(tv, tx, ty, ta, thor, distance);
    }
  }

  @Override
  public void disabledPeriodic() {setLedState(1);}

  public double valueDeadZone(double in, double deadValue)
  {
    if (Math.abs(in) < deadValue) {return 0;}
    else                          {return in;}
  }

  public void setDriveMode(NeutralMode mode)
  {
    leftMaster.setNeutralMode(mode);
    rightMaster.setNeutralMode(mode);
    leftSlave.setNeutralMode(mode);
    rightSlave.setNeutralMode(mode);
  }

  public void setDriveSensorPhase(boolean leftPhase, boolean rightPhase)
  {
    if(driveTrain != null)
    {
      leftMaster.setSensorPhase(leftPhase);
      leftSlave.setSensorPhase(leftPhase);
      rightMaster.setSensorPhase(rightPhase);
      rightSlave.setSensorPhase(rightPhase);
    }
  }

  public double getIndexSensorIn() {System.out.println("WARNING! indexIn: " + (indexSensorIn.getRange() - indexSensorIn.getRangeSigma()) / 25.4); return indexSensorIn.getRange() / 25.4;}
  public double getIndexSensorOut() {System.out.println("WARNING! indexOut: " + (indexSensorOut.getRange() - indexSensorOut.getRangeSigma()) / 25.4); return indexSensorOut.getRange() / 25.4;}
  public void setPipeline(double pipeline) {if(Constants.USING_LIMELIGHT) {pipelineEntry.setNumber(pipeline);}}
  public void setLedState(double state) {if(Constants.USING_LIMELIGHT) {ledModeEntry.setNumber(state);}}
  public void intakeBall() {intakeMaster.set(Constants.intakeSpeed);}
  public void stopIntake() {intakeMaster.set(0);}
  public void spinShooter() {shooterMaster.set(Constants.shooterSpeed);}
  public void stopShooter() {shooterMaster.set(0);}
  public void moveIndexer() {indexer.set(Constants.indexerSpeed);}
  public void stopIndexer() {indexer.set(0);}
  public void moveConveyorBelt() {conveyorBelt.set(Constants.conveyorBeltSpeed);}
  public void stopConveyorBelt() {conveyorBelt.set(0);}
  public void setRobotColor(double color) {rgbController.set(color);}

  public int warmupCounter()
  {
    shooterCounter += 1;
    return shooterCounter;
  }

  public void moveClimber()
  {
    climberMaster.set(Constants.climbingSpeed);
    climberSlave.set(-Constants.climbingSpeed);
  }

  public void stopClimber()
  {
    climberMaster.set(0);
    climberSlave.set(0);
  }

  public void resetSensors()
  {
    if(Constants.USING_GYRO && navx != null) {navx.reset();}
    rightMaster.setSelectedSensorPosition(0);
    leftMaster.setSelectedSensorPosition(0);
  }

  public double getDistance()
  {
    if(Constants.USING_LIMELIGHT)
    {
      setLedState(0);
      double thor = targetHorizontalEntry.getDouble(0.0);
      double distance = (Constants.physicalLength * Constants.pxDistance) / thor;
      return distance;
    }
    else {return 0;}
  }

  public void positionRobot(double xPos, double area,
                            double xThresh, double areaThresh, double areaAddition,
                            double centeringSpeed, double movementSpeed)
  {
    if(Constants.USING_LIMELIGHT)
    {
      double xSpeed = 0.0;
      double ySpeed = 0.0;

      setLedState(0);

      // Set rotation speed
      if(xPos >= -xThresh && xPos <= xThresh){}
      else if(xPos <= -xThresh) {xSpeed = -centeringSpeed;}
      else if(xPos >= xThresh)  {xSpeed = centeringSpeed;}

      // Set movement speed
      if(area >= areaThresh && area <= areaThresh + areaAddition){}
      else if(area <= areaThresh)                 {ySpeed = movementSpeed;}
      else if(area >= areaThresh + areaAddition)  {ySpeed = -movementSpeed;}

      driveTrain.arcadeDrive(xSpeed, ySpeed);
    }
  }

  public void outputDebug(double tv, double tx, double ty, double ta, double thor, double distance)
  {
    if(Constants.USING_DEBUG)
    {
      System.out.println("WARNING! tv: " + tv);
      System.out.println("WARNING! tx: " + tx);
      System.out.println("WARNING! ty: " + ty);
      System.out.println("WARNING! ta: " + ta);
      System.out.println("WARNING! thor: " + thor);
      System.out.println("WARNING! distance: " + distance);
      System.out.println("WARNING! rightMaster: " + rightMaster.getSelectedSensorPosition());
      System.out.println("WARNING! leftMaster: " + leftMaster.getSelectedSensorPosition());
      System.out.println("WARNING! gyro.getAngle(): " + navx.getAngle());
    }
  }
}
