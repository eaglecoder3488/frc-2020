package frc.robot;

import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import java.util.Arrays;
import edu.wpi.first.wpilibj.controller.RamseteController;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.trajectory.TrajectoryGenerator;

public class RobotContainer
{
    private DriveTrain driveTrain;

    public RobotContainer() {driveTrain = new DriveTrain();}

    public void resetGyro() {driveTrain.resetGyro();}

    private Trajectory createTrajectory()
    {
        TrajectoryConfig config = new TrajectoryConfig(Constants.maxVelocity, Constants.maxAcceleration);
        config.setKinematics(driveTrain.getKinematics());
        
        /*
        // Create a voltage constraint to ensure we don't accelerate too fast
        var autoVoltageConstraint = new DifferentialDriveVoltageConstraint( 
            driveTrain.getSimpleMotorFeedForward(),
            driveTrain.getKinematics(),
            10
        );
           
        config.addConstraint(autoVoltageConstraint);
        */
        
        Trajectory tempTrajectory = TrajectoryGenerator.generateTrajectory(
            Arrays.asList(new Pose2d(), new Pose2d(Units.feetToMeters(10), 0, new Rotation2d())),
            config
        );

        return tempTrajectory;
    }

    public Command getAutonomousCommand()
    {
        driveTrain.resetOdometry(new Pose2d());

        Trajectory trajectory = createTrajectory();

        RamseteCommand command = new RamseteCommand(
            trajectory,
            driveTrain::getPose,
            new RamseteController(Constants.kRamseteB, Constants.kRamseteZeta),
            driveTrain.getSimpleMotorFeedForward(),
            driveTrain.getKinematics(),
            driveTrain::getSpeeds,
            driveTrain.getLeftPIDCOntroller(),
            driveTrain.getRightPIDCOntroller(),
            driveTrain::tankDriveVolts,
            driveTrain
        );
        
        return command.andThen(() -> driveTrain.tankDriveVolts(0, 0));
    }
}