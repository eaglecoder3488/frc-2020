package frc.robot;

import com.playingwithfusion.TimeOfFlight.RangingMode;
import edu.wpi.first.wpilibj.util.Units;

public final class Constants
{
    // Subsystem controls
    public static final boolean USING_DEBUG = true;
    public static final boolean USING_FRONT_CAMERA = false;
    public static final boolean USING_BACK_CAMERA = false;
    public static final boolean USING_LIMELIGHT = false;
    public static final boolean USING_PATHWEAVER = false;
    public static final boolean USING_GYRO = false;
    public static final boolean USING_INDEX_SENSORS = true;
    public static final boolean USING_RGB = true;
 
    // Motor config
    public static final boolean LEFT_INVERTED = true;
    public static final boolean LEFT_PHASE = false; // NEEDS TO BE CHANGED FOR TALONFX
    public static final boolean RIGHT_INVERTED = true;
    public static final boolean RIGHT_PHASE = false; // NEEDS TO BE CHANGED FOR TALONFX, REVERSED ENCODER

    // Gyro config
    public static final boolean GYRO_REVERSED = false;

    // CAN IDs of DriveTrain
    public static final int BACK_RIGHT = 4;
    public static final int FRONT_RIGHT = 3;
    public static final int FRONT_LEFT = 2;
    public static final int BACK_LEFT = 1;

    /*
    5, 6, 7, 12 - Victor
    8, 9, 10, 11 - Talon
    */

    // CAN IDs of Mechanisms
    public static final int CLIMBER_MASTER = 6;
    public static final int CLIMBER_SLAVE = 5;
    public static final int SHOOTER = 8;
    public static final int INTAKE = 12;
    public static final int INDEX_IN = 7;
    public static final int INDEX_OUT = 9;
    public static final int SPINNER_A = 10;
    public static final int SPINNER_B = 11;

    // CAN IDs of Sensors
    public static final int INDEX_SENSOR_IN = 13;
    public static final int INDEX_SENSOR_OUT = 14;

    // Literally the most important CAN ID
    public static final int RGB_CONTROLLER = 0;

    // Color of robot
    public static final double robotColor = 0.79; // Blue Green
    public static final double debugColor = 0.67; // Gold
    public static final double warningColor = 0.61; // Red

    // Sensor Settings
    public static final int sensorRate = 24; // Polling Rate (minimum of 24ms, maximum of 1000ms)
    public static final RangingMode sensorMode = RangingMode.Long; // Two modes, short and long
    public static final double sensorInThresh = 5.0; // Inches
    public static final double indexCapacity = 3;
    public static final boolean indexEncoderPhase = false;

    // Shooter Warmup Delay. Based on default timed 20ms polling rate
    public static final int warmupTime = 75; // +1 for every 20ms (.02s); +5 per .1s; +25 per .5s; +50 per 1s
    public static final int indexerTime = 25; // Add this to warmupTime

    // Other device locations
    public static final int CONTROLLER_PORT = 0;
    public static final int FRONT_CAMERA_PORT = 0;
    public static final int BACK_CAMERA_PORT = 1;

    // Dead zone value
    public static final double DEAD_ZONE_VALUE = 0.3;

    // DPAD is dump
    public static final int dPadRight = 90;
    public static final int dPadLeft = 270;
    public static final int dPadUp = 0;
    public static final int dPadDown = 180;

    // Xbox Axis
    public static final int xAxis = 0; // LStick: 0; RStick: 4
    public static final int yAxis = 1; // LStick: 1; RStick: 5

    // Mechanism Speeds
    public static final double climbingSpeed = 0.15; // ONLY MOVES IN ONE DIRECTION.
    public static final double intakeSpeed = 0.6;
    public static final double shooterSpeed = -0.65; //-.6
    public static final double conveyorBeltSpeed = 0.6;
    public static final double indexerSpeed = 0.9;

    // Physical robot properties
    public static final double kGearRatio = 10.75;
    public static final double kWheelRadiusMeters = Units.inchesToMeters(4.0);
    public static final double kTrackWidthMeters = Units.inchesToMeters(22.5625);

    // Encoder properties
    public static final double sensorUnitsPerRevolution = 2048;
    public static final double wheelUnitsPerRevolution = 22016;
    public static final double pulsePerMeter = (sensorUnitsPerRevolution / (2 * kWheelRadiusMeters * Math.PI / kGearRatio));
    public static final double indexRevolutionValue = 4096;

    // Characterization constants
    public static final double kS = 0.186;
    public static final double kV = 1.84;
    public static final double kA = 0.218;
    public static final double kP = 0.000615;
    public static final double kI = 0;
    public static final double kD = 0.000268;

    // Trajectory Speeds
    public static final double maxVelocity = Units.feetToMeters(3.0);
    public static final double maxAcceleration = Units.feetToMeters(3.0);

    // Ramsete Controller Values
    public static final double kRamseteB = 2;
    public static final double kRamseteZeta = 0.7;

    // Limelight Vision Variables
    public static final double xThresh = 2;
    public static final double areaThresh = .2;
    public static final double areaAddition = .8;
    public static final double movementSpeed = .5;
    public static final double centeringSpeed = .5;

    // Distance Calculation Variables (Similar Triangles)
    public static final double physicalDistance = 56.25; // Inches
    public static final double physicalLength = 15.5; // Inches
    public static final double pxLength = 72; // Res: 320x240... Lowers accuracy
    public static final double pxDistance = (physicalDistance * pxLength) / physicalLength;

    // Default limelight variables
    public static final double pipeline = 1;  // 0 - Default, 1 - Tape, 2 - Power Cell
    public static final double defaultLedState = 1; // 0 - Pipeline, 1 - Off, 2 - Blink, 3 - On
}