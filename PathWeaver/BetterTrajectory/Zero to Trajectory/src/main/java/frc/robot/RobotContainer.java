package frc.robot;

import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.controller.RamseteController;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj.trajectory.constraint.DifferentialDriveVoltageConstraint;

public class RobotContainer
{
    private DriveTrain driveTrain;

    public RobotContainer()
    {
        driveTrain = new DriveTrain();
    }

    public void resetGyro()
    {
        driveTrain.resetGyro();
    }

    
    private Trajectory createTrajectory()
    {
        TrajectoryConfig config = new TrajectoryConfig( Units.feetToMeters( 6 ), Units.feetToMeters( 4 ) );
        config.setKinematics( driveTrain.getKinematics() );
        
        /*
        // Create a voltage constraint to ensure we don't accelerate too fast
        var autoVoltageConstraint = new DifferentialDriveVoltageConstraint( 
            driveTrain.getSimpleMotorFeedForward(),
            driveTrain.getKinematics(),
            10
        );
           
        config.addConstraint( autoVoltageConstraint );
        */
        
        Trajectory tempTrajectory = TrajectoryGenerator.generateTrajectory(
            Arrays.asList( new Pose2d(), new Pose2d( 5, 0, new Rotation2d() ) ),

            config
        );

        Trajectory trajectory = TrajectoryGenerator.generateTrajectory(
        // Start at the origin facing the +X direction
        new Pose2d(0, 0, new Rotation2d(0)),
        // Pass through these two interior waypoints, making an 's' curve path
        List.of(
            new Translation2d(1, 1),
            new Translation2d(2, -1)
        ),
        // End 3 meters straight ahead of where we started, facing forward
        new Pose2d(3, 0, new Rotation2d(0)),
        // Pass config
        config
    );
        

        return trajectory;
    }

    public Command getAutonomousCommand()
    {
        driveTrain.resetOdometry( new Pose2d() );

        Trajectory trajectory = createTrajectory();

        RamseteCommand command = new RamseteCommand(
            trajectory,
            driveTrain::getPose,
            new RamseteController( DriveTrain.kRamseteB, DriveTrain.kRamseteZeta ),
            driveTrain.getSimpleMotorFeedForward(),
            driveTrain.getKinematics(),
            driveTrain::getSpeeds,
            driveTrain.getLeftPIDCOntroller(),
            driveTrain.getRightPIDCOntroller(),
            driveTrain::tankDriveVolts,
            driveTrain
        );

        RamseteCommand commandB = new RamseteCommand(
            trajectory,
            driveTrain::getPose,
            new RamseteController( DriveTrain.kRamseteB, DriveTrain.kRamseteZeta ),
            driveTrain.getSimpleMotorFeedForward(),
            driveTrain.getKinematics(),
            driveTrain::getSpeeds,
            driveTrain.getLeftPIDCOntroller(),
            driveTrain.getRightPIDCOntroller(),
            driveTrain::tankDriveVolts,
            driveTrain
        );

        return command.andThen( () -> driveTrain.tankDriveVolts( 0, 0 ) );
    }

    public Command test()
    {
		return null;
        
    }
}