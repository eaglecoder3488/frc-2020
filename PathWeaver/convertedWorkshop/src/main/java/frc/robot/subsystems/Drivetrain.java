package frc.robot.subsystems;

import com.kauailabs.navx.frc.AHRS;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Drivetrain extends SubsystemBase
{
  private static final double kGearRatio = 10.71;
  private static final double kWheelRadiusInches = 3.0;

  WPI_TalonSRX leftMaster = new WPI_TalonSRX(1);
  WPI_TalonSRX rightMaster = new WPI_TalonSRX(3);

  WPI_TalonSRX leftSlave = new WPI_TalonSRX(2);
  WPI_TalonSRX rightSlave = new WPI_TalonSRX(4);

  AHRS navx = new AHRS(SPI.Port.kMXP);

  DifferentialDriveKinematics kinematics = new DifferentialDriveKinematics(Units.inchesToMeters(18.75));
  DifferentialDriveOdometry odometry = new DifferentialDriveOdometry(getHeading());

  SimpleMotorFeedforward feedforward = new SimpleMotorFeedforward(0.749, 0.0782, 0.0142);

  PIDController leftPIDController = new PIDController(2.95, 0, 0);
  PIDController rightPIDController = new PIDController(2.95, 0, 0);

  Pose2d pose = new Pose2d();

  public Drivetrain()
  {
    leftMaster.setInverted(false);
    leftMaster.setSensorPhase(true);
    rightMaster.setInverted(false);
    rightMaster.setSensorPhase(true);
    
    leftSlave.follow(leftMaster);
    rightSlave.follow(rightMaster);

    navx.reset();
  }

  public Rotation2d getHeading()
  {
    return Rotation2d.fromDegrees(-navx.getAngle());
  }

  public DifferentialDriveWheelSpeeds getSpeeds()
  {
    return new DifferentialDriveWheelSpeeds(
        leftMaster.getSelectedSensorVelocity() / kGearRatio * 2 * Math.PI * Units.inchesToMeters(kWheelRadiusInches) / 60,
        rightMaster.getSelectedSensorVelocity() / kGearRatio * 2 * Math.PI * Units.inchesToMeters(kWheelRadiusInches) / 60
    );
  }

  public double getLeftSpeed()
  {
      return leftMaster.getSelectedSensorVelocity() / kGearRatio * 2 * Math.PI * Units.inchesToMeters(kWheelRadiusInches) / 60;
  }

  public double getRightSpeed()
  {
    return rightMaster.getSelectedSensorVelocity() / kGearRatio * 2 * Math.PI * Units.inchesToMeters(kWheelRadiusInches) / 60;  
  }

  public DifferentialDriveKinematics getKinematics()
  {
    return kinematics;
  }

  public Pose2d getPose()
  {
    return pose;
  }

  public SimpleMotorFeedforward getFeedforward()
  {
    return feedforward;
  }

  public PIDController getLeftPIDController()
  {
    return leftPIDController;
  }

  public PIDController getRightPIDController()
  {
    return rightPIDController;
  }

  public void setOutputVolts(double leftVolts, double rightVolts)
  {
    leftMaster.set(leftVolts / 12);
    rightMaster.set(rightVolts / 12);
  }

  public void reset()
  {
    odometry.resetPosition(new Pose2d(), getHeading());
  }

  @Override
  public void periodic()
  {
    pose = odometry.update(getHeading(), getLeftSpeed(), getRightSpeed());
  }
}
