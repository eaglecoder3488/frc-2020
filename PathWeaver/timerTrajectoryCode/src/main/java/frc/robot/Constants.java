package frc.robot;

import com.playingwithfusion.TimeOfFlight.RangingMode;
import edu.wpi.first.wpilibj.util.Units;

public final class Constants
{
    // Subsystem controls
    public static final boolean USING_DEBUG = true;
    public static final boolean USING_CAMERA = false;
    public static final boolean USING_LIMELIGHT = false;
    public static final boolean USING_PATHWEAVER = false;
    public static final boolean USING_GYRO = true;
    public static final boolean DISPLAYING_ENCODERS = true;
    public static final boolean USING_MECHANISM_CONTROLS = true;
    public static final boolean USING_CLIMBER = true;
    public static final boolean USING_INTAKE = true;
    public static final boolean USING_SHOOTER = true;
    public static final boolean USING_INDEXER = true;
    public static final boolean USING_INDEX_SENSORS = false;
    public static final boolean USING_RGB = true;
 
    // Motor config
    public static final boolean LEFT_INVERTED = true;
    public static final boolean LEFT_PHASE = false; // NEEDS TO BE CHANGED FOR TALONFX
    public static final boolean RIGHT_INVERTED = true;
    public static final boolean RIGHT_PHASE = false; // NEEDS TO BE CHANGED FOR TALONFX, REVERSED ENCODER

    // Gyro config
    public static final boolean GYRO_REVERSED = false;

    // CAN IDs of DriveTrain
    public static final int BACK_RIGHT = 4;
    public static final int FRONT_RIGHT = 3;
    public static final int FRONT_LEFT = 2;
    public static final int BACK_LEFT = 1;

    /*
    5, 6, 7, 12 - Victor
    8, 9, 10, 11 - Talon
    */

    // CAN IDs of Mechanisms
    public static final int CLIMBER_MASTER = 6;
    public static final int CLIMBER_SLAVE = 5;
    public static final int SHOOTER = 8;
    public static final int INTAKE = 12;
    public static final int INDEX_IN = 7;
    public static final int INDEX_OUT = 9;
    public static final int COLOR_WHEEL_A = 10;
    public static final int COLOR_WHEEL_B = 11;

    // CAN IDs of Sensors
    public static final int INDEX_SENSOR_IN = 13;
    public static final int INDEX_SENSOR_OUT = 14;

    // Literally the most important CAN ID
    public static final int RGB_CONTROLLER = 0;

    // Color of robot
    public static final double robotColor = 0.79;

    // Sensor Settings
    public static final int sensorRate = 24; // Polling Rate (minimum of 24ms, maximum of 1000ms)
    public static final RangingMode sensorMode = RangingMode.Short; // Two modes, short and long
    public static final double sensorThresh = .5;
    public static final boolean indexEncoderPhase = false;

    // Other device locations
    public static final int CONTROLLER_PORT = 0;
    public static final int CAMERA_PORT = 0;

    // Dead zone value
    public static final double DEAD_ZONE_VALUE = 0.1;

    // Mechanism Speeds
    public static final double climbingSpeed = 0.25;
    public static final double intakeSpeed = 0.45;
    public static final double shooterSpeed = .75;
    public static final double conveyorBeltSpeed = .5;
    public static final double indexerSpeed = .5;

    // Physical robot properties
    public static final double kGearRatio = 10.75;
    public static final double kWheelRadiusMeters = Units.inchesToMeters(4.0);
    public static final double kTrackWidthMeters = Units.inchesToMeters(22.5625);

    // Encoder properties
    public static final double sensorUnitsPerRevolution = 2048;
    public static final double wheelUnitsPerRevolution = 22016;
    public static final double indexRevolutionValue = 4096;

    // Characterization constants
    public static final double ks = 0.186;
    public static final double kv = 1.84;
    public static final double ka = 0.218;
    public static final double kP = 0.000615;
    public static final double kI = 0;
    public static final double kD = 0.000268;

    // Trajectory Speeds
    public static final double maxVelocity = Units.feetToMeters(3.0);
    public static final double maxAcceleration = Units.feetToMeters(3.0);

    // Limelight Vision Variables
    public static final double xThresh = 2;
    public static final double areaThresh = .2;
    public static final double areaAddition = .8;
    public static final double movementSpeed = .5;
    public static final double centeringSpeed = .5;

    // Distance Calculation Variables (Similar Triangles)
    public static final double physicalDistance = 56.25; // Inches
    public static final double physicalLength = 15.5; // Inches
    public static final double pxLength = 72; // Res: 320x240... Lowers accuracy
    public static final double pxDistance = (physicalDistance * pxLength) / physicalLength;

    // Default limelight variables
    public static final double pipeline = 1;  // 0 - Default, 1 - Tape, 2 - Power Cell
    public static final double defaultLedState = 1; // 0 - Pipeline, 1 - Off, 2 - Blink, 3 - On
}