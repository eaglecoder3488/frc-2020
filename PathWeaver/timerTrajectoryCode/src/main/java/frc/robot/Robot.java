/*
TO-DO
  [ ] Autonomous:
    [ ] Check limelight functionality w/getBButton()
    [ ] Check if limelight pipeline and led control is fuctional
    [ ] Recalibrate Distance Formula
    [ ] Check if trajectories are running
    [ ] Generate own trajectory and run it
    [ ] Integrate limelight with trajectory
    [ ] How to invert encoders properly?
*/

package frc.robot;

import frc.robot.Constants;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;
import com.playingwithfusion.TimeOfFlight;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj2.command.CommandScheduler;

public class Robot extends TimedRobot
{
  RobotContainer container;

  XboxController xboxPad;
  UsbCamera camera;
  AHRS navx;

  WPI_TalonFX leftMaster;
  WPI_TalonFX rightMaster;
  WPI_TalonFX leftSlave;
  WPI_TalonFX rightSlave;

  SpeedControllerGroup leftMotors;
  SpeedControllerGroup rightMotors;

  DifferentialDrive driveTrain;

  WPI_VictorSPX climberMaster;
  WPI_VictorSPX climberSlave;

  WPI_VictorSPX intakeMaster;

  WPI_VictorSPX conveyorBelt;
  WPI_TalonSRX indexer;

  WPI_TalonSRX shooterMaster;

  Spark rgbController;

  TimeOfFlight indexSensorIn;
  TimeOfFlight indexSensorOut;

  NetworkTable netTable;
  NetworkTableInstance netInst;

  NetworkTableEntry targetStateEntry;
  NetworkTableEntry targetXOffsetEntry;
  NetworkTableEntry targetYOffsetEntry;
  NetworkTableEntry targetAreaEntry;
  NetworkTableEntry targetHorizontalEntry;
  NetworkTableEntry pipelineEntry; 
  NetworkTableEntry ledModeEntry;

  @Override
  public void robotInit()
  {
    if(xboxPad == null)
    {
      xboxPad = new XboxController(Constants.CONTROLLER_PORT);
    }
    
    if(driveTrain == null)
    {
      leftMaster = new WPI_TalonFX(Constants.FRONT_LEFT);
      rightMaster = new WPI_TalonFX(Constants.BACK_RIGHT);
      leftSlave = new WPI_TalonFX(Constants.BACK_LEFT);
      rightSlave = new WPI_TalonFX(Constants.FRONT_RIGHT);

      leftMaster.setSensorPhase(Constants.LEFT_PHASE);
      leftSlave.setSensorPhase(Constants.LEFT_PHASE);
      rightMaster.setSensorPhase(Constants.RIGHT_PHASE);
      rightSlave.setSensorPhase(Constants.RIGHT_PHASE);

      leftMaster.setNeutralMode(NeutralMode.Brake);
      leftSlave.setNeutralMode(NeutralMode.Brake);
      rightMaster.setNeutralMode(NeutralMode.Brake);
      rightSlave.setNeutralMode(NeutralMode.Brake);

      leftMotors = new SpeedControllerGroup(leftMaster, leftSlave);
      rightMotors = new SpeedControllerGroup(rightMaster, rightSlave);

      leftMotors.setInverted(Constants.LEFT_INVERTED);
      rightMotors.setInverted(Constants.RIGHT_INVERTED);

      driveTrain = new DifferentialDrive(leftMotors, rightMotors);
    }

    if(climberMaster == null && Constants.USING_CLIMBER)
    {
      climberMaster = new WPI_VictorSPX(Constants.CLIMBER_MASTER);
      climberSlave = new WPI_VictorSPX(Constants.CLIMBER_SLAVE);
    }

    if(intakeMaster == null && Constants.USING_INTAKE)
    {
      intakeMaster = new WPI_VictorSPX(Constants.INTAKE);
    }

    if(shooterMaster == null && Constants.USING_SHOOTER)
    {
      shooterMaster = new WPI_TalonSRX(Constants.SHOOTER);
    }

    if(conveyorBelt == null && Constants.USING_INDEXER)
    {
      conveyorBelt = new WPI_VictorSPX(Constants.INDEX_IN);
      indexer = new WPI_TalonSRX(Constants.INDEX_OUT);
    }

    if(indexSensorIn == null && Constants.USING_INDEX_SENSORS)
    {
      indexSensorIn = new TimeOfFlight(Constants.INDEX_SENSOR_IN);
      indexSensorOut = new TimeOfFlight(Constants.INDEX_SENSOR_OUT);
      indexSensorIn.setRangingMode(Constants.sensorMode, Constants.sensorRate);
      indexSensorOut.setRangingMode(Constants.sensorMode, Constants.sensorRate);
    }

    if(navx == null && Constants.USING_GYRO)
    {
      navx = new AHRS(SPI.Port.kMXP);
      navx.reset();
    }

    if(rgbController == null && Constants.USING_RGB)
    {
      rgbController = new Spark(Constants.RGB_CONTROLLER);
    }

    if(container == null && Constants.USING_PATHWEAVER)
    {
      container = new RobotContainer();
    }

    if(netTable == null && Constants.USING_LIMELIGHT)
    {
      netTable = NetworkTableInstance.getDefault().getTable("limelight");
      targetStateEntry = netTable.getEntry("tv");
      targetXOffsetEntry = netTable.getEntry("tx");
      targetYOffsetEntry = netTable.getEntry("ty");
      targetAreaEntry = netTable.getEntry("ta");
      targetHorizontalEntry = netTable.getEntry("thor");
      pipelineEntry = netTable.getEntry("pipeline");
      ledModeEntry = netTable.getEntry("ledMode");
      setPipeline(Constants.pipeline);
      setLedState(1); // Keep the linelight leds off during initialization
    }

    if(camera == null && Constants.USING_CAMERA)
    {
      camera = CameraServer.getInstance().startAutomaticCapture(Constants.CAMERA_PORT);
      System.out.println("WARNING! Camera Initializing!");
    }
  }

  @Override
  public void teleopInit()
  {
    if(Constants.USING_PATHWEAVER)
      container.reset();
  }

  @Override
  public void teleopPeriodic()
  {
    double x = valueDeadZone(xboxPad.getRawAxis(4), Constants.DEAD_ZONE_VALUE);
    double y = valueDeadZone(xboxPad.getRawAxis(1), Constants.DEAD_ZONE_VALUE);
    driveTrain.arcadeDrive(y, -x);

    if(Constants.USING_MECHANISM_CONTROLS)
    {
      if(Constants.USING_INTAKE)
      {
        if(xboxPad.getAButton())
        {
          intakeBall();
          moveConveyorBelt();
        }
        else
        {
          stopIntake();
          // Don't stop the conveyor belt because the sensor will stop it.
        }
      }

      if(Constants.USING_SHOOTER) // Fights with sensor state
      {
        if(xboxPad.getBButton())
        {
          shootBall();
        }
        else
        {
          stopShooter();
        }
      }

      if(Constants.USING_INDEXER && Constants.USING_INDEX_SENSORS) // Fights with button state
      { 
        if(getIndexSensorIn() <= Constants.sensorThresh)
        {
          // If sensor is tripped, run the indexer. If the outer sensor is tripped, run the shooter as well.
          moveConveyorBelt();
          moveIndexer(); // Fights with getIndexSensorOut else statement

          if(getIndexSensorOut() <= Constants.indexerSpeed)
          {
            moveIndexer();
            shootBall();
          }
          else
          {
            stopIndexer(); // Fights
            stopShooter(); // Fights
          }
        }
        else
        {
          stopConveyorBelt();
        }
      }

      if(Constants.USING_INDEXER && !Constants.USING_INDEX_SENSORS)
      {
        if(xboxPad.getYButton())
        {
          moveConveyorBelt();
          moveIndexer();
        }
        else
        {
          stopConveyorBelt();
          stopIndexer();
        }
      }

      if(Constants.USING_CLIMBER)
      {
        if(xboxPad.getBumper(Hand.kLeft))
        {
          moveClimberDown();
        }
        else if(xboxPad.getBumper(Hand.kRight))
        {
          moveClimberUp();
        }
        else
        {
          stopClimber();
        }
      }
    }
    else
    {
      if(xboxPad.getBButton())
      {
        double tx = targetXOffsetEntry.getDouble(0.0);
        double ta = targetAreaEntry.getDouble(0.0);
        positionRobot(tx, ta, Constants.xThresh, Constants.areaThresh, Constants.areaAddition, Constants.centeringSpeed, Constants.movementSpeed);
      }
      else
      {
        setLedState(1);
      }

      if(xboxPad.getYButton())
      {
        System.out.println("WARNING! Distance away from target: " + getDistance());
      }
      else
      {
        setLedState(1);
      }

      if(xboxPad.getAButton())
      {
        resetEncoders();
      }
      
      if(xboxPad.getXButton())
      {
        setDriveMode(NeutralMode.Coast);
      }
      else
      {
        setDriveMode(NeutralMode.Brake);
      }
    }

    if(Constants.DISPLAYING_ENCODERS)
    {
      System.out.println("WARNING! rightFront: " + rightMaster.getSelectedSensorPosition());
      System.out.println("WARNING! leftBack: " + leftMaster.getSelectedSensorPosition());
    }

    if(Constants.USING_GYRO)
    {
      System.out.println("WARNING! gyro.getAngle(): " + navx.getAngle());
    }
  }

  @Override
  public void autonomousInit()
  {
    if(Constants.USING_PATHWEAVER)
    {
      container.reset();
      container.getAutonomousCommand().schedule();
    }
  }

  @Override
  public void autonomousPeriodic()
  {
    if(Constants.USING_PATHWEAVER)
    {
      CommandScheduler.getInstance().run();
    }
  }

  @Override
  public void testPeriodic()
  {
    if(Constants.USING_LIMELIGHT)
    {
      double tv = targetStateEntry.getDouble(0.0);
      double tx = targetXOffsetEntry.getDouble(0.0);
      double ty = targetYOffsetEntry.getDouble(0.0);
      double ta = targetAreaEntry.getDouble(0.0);
      double thor = targetHorizontalEntry.getDouble(0.0);
      double distance = getDistance();
      positionRobot(tx, ta, Constants.xThresh, Constants.areaThresh, Constants.areaAddition, Constants.centeringSpeed, Constants.movementSpeed);
      outputDebug(tv, tx, ty, ta, thor, distance);
    }

    if(Constants.USING_RGB)
    {
      rgbController.set((navx.getAngle() / 360));
      if(navx.getAngle() >= 360)
      {
        navx.reset();
      }
    }
  }

  @Override
  public void disabledPeriodic()
  {
    if(Constants.USING_PATHWEAVER)
    {
      CommandScheduler.getInstance().run();
    }
    setLedState(1);
  }

  public double valueDeadZone(double in, double deadValue)
  {
    if (Math.abs(in) < deadValue)
    {
      return 0;
    }
    else
    {
      return in;
    }
  }

  public void setDriveMode(NeutralMode mode)
  {
    leftMaster.setNeutralMode(mode);
    rightMaster.setNeutralMode(mode);
    leftSlave.setNeutralMode(mode);
    rightSlave.setNeutralMode(mode);
  }

  public void intakeBall()
  {
    intakeMaster.set(Constants.intakeSpeed);
  }

  public void stopIntake()
  {
    intakeMaster.set(0);
  }

  public void shootBall()
  {
    shooterMaster.set(Constants.shooterSpeed);
  }

  public void stopShooter()
  {
    shooterMaster.set(0);
  }

  public void moveIndexer()
  {
    indexer.set(Constants.conveyorBeltSpeed);
  }

  public void stopIndexer()
  {
    indexer.set(0);
  }

  public void moveConveyorBelt()
  {
    conveyorBelt.set(Constants.conveyorBeltSpeed);
  }

  public void stopConveyorBelt()
  {
    conveyorBelt.set(0);
  }
  
  public void moveClimberUp()
  {
    climberMaster.set(Constants.climbingSpeed);
    climberSlave.set(Constants.climbingSpeed);
  }

  public void moveClimberDown()
  {
    climberMaster.set(-Constants.climbingSpeed);
    climberSlave.set(-Constants.climbingSpeed);
  }

  public void stopClimber()
  {
    climberMaster.set(0);
    climberSlave.set(0);
  }

  public void resetEncoders()
  {
    if(Constants.DISPLAYING_ENCODERS)
    {
      rightMaster.setSelectedSensorPosition(0);
      leftMaster.setSelectedSensorPosition(0);
    }

    if(Constants.USING_GYRO)
    {
      navx.reset();
    }
  }

  public double getIndexSensorIn()
  {
    // Convert from mm to inches + compensate for sensor error
    return (indexSensorIn.getRange() - indexSensorIn.getRangeSigma()) / 25.4;
  }

  public double getIndexSensorOut()
  {
    // Convert from mm to inches + compensate for sensor error
    return (indexSensorOut.getRange() - indexSensorOut.getRangeSigma()) / 25.4;
  }

  public void setPipeline(double pipeline)
  {
    if(Constants.USING_LIMELIGHT)
    {
      pipelineEntry.setNumber(pipeline);
    }
  }

  public void setLedState(double state)
  {
    if(Constants.USING_LIMELIGHT)
    {
      ledModeEntry.setNumber(state);
    }
  }

  public double getDistance()
  {
    if(Constants.USING_LIMELIGHT)
    {
      setLedState(0);
      double thor = targetHorizontalEntry.getDouble(0.0);
      double distance = (Constants.physicalLength * Constants.pxDistance) / thor;
      return distance;
    }
    else
    {
      return 0;
    }
  }

  public void positionRobot(double xPos, double area,
                            double xThresh, double areaThresh, double areaAddition,
                            double centeringSpeed, double movementSpeed)
  {
    if(Constants.USING_LIMELIGHT)
    {
      double xSpeed = 0.0;
      double ySpeed = 0.0;

      setLedState(0);

      // Set rotation speed
      if(xPos >= -xThresh && xPos <= xThresh){}
      else if(xPos <= -xThresh) {xSpeed = -centeringSpeed;}
      else if(xPos >= xThresh)  {xSpeed = centeringSpeed;}

      // Set movement speed
      if(area >= areaThresh && area <= areaThresh + areaAddition){}
      else if(area <= areaThresh)                 {ySpeed = movementSpeed;}
      else if(area >= areaThresh + areaAddition)  {ySpeed = -movementSpeed;}

      driveTrain.arcadeDrive(xSpeed, ySpeed);
    }
  }

  public void outputDebug(double tv, double tx, double ty, double ta, double thor, double distance)
  {
    if(Constants.USING_DEBUG)
    {
      System.out.println("WARNING! tv: " + tv);
      System.out.println("WARNING! tx: " + tx);
      System.out.println("WARNING! ty: " + ty);
      System.out.println("WARNING! ta: " + ta);
      System.out.println("WARNING! thor: " + thor);
      System.out.println("WARNING! distance: " + distance);
    }
  }
}
