/*
TO-DO LIST
[ ] Regenerate characterization for robot
[X] SWAP WIRES
[ ] Setup PathWeaver 2020
  [ ] Get peak velocity and acceleration
[ ] Check if this trajectory works
  [ ] If not, check encoders
[ ] Transfer Current Timed Code
  - Maybe a copy paste might do it? Things could break...
*/
package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.Subsystem;

public class Robot extends TimedRobot
{
  RobotContainer container;

  @Override
  public void robotInit()
  {
    container = new RobotContainer();
  }

  @Override
  public void robotPeriodic()
  {

  }

  @Override
  public void disabledInit()
  {

  }

  @Override
  public void disabledPeriodic()
  {
    CommandScheduler.getInstance().run();
  }

  @Override
  public void autonomousInit()
  {
    container.reset();
    container.getAutonomousCommand().schedule();
  }

  @Override
  public void autonomousPeriodic()
  {
    CommandScheduler.getInstance().run();
  }

  @Override
  public void teleopInit()
  {
    container.reset();
  }

  @Override
  public void teleopPeriodic()
  {

  }

  @Override
  public void testInit()
  {

  }

  @Override
  public void testPeriodic()
  {

  }
}
