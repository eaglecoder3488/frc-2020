package frc.robot;

import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.util.Units;

public final class Constants
{
    // Constants found from characterization
    public static final double ksVolts = 0.749;
    public static final double kvVoltSecondsPerMeter = 0.0782;
    public static final double kaVoltSecondsSquaredPerMeter = 0.0142;
    public static final double kPDriveVelocity = 2.95;

    // Drive Train Width
    public static final double kTrackWidthMeters = Units.inchesToMeters(18.75);
    public static final DifferentialDriveKinematics kDriveKinematics = new DifferentialDriveKinematics(kTrackWidthMeters);

    // Other important pathweaver stuff
    public static final double kGearRatio = 10.71;
    public static final double kWheelRadiusInches = 3.0;

    // Not extremely crucial
    public static final double kMaxSpeedMetersPerSecond = 1;
    public static final double kMaxAccelerationMetersPerSecondSquared = .5;

    // Reasonable baseline values for a RAMSETE follower in units of meters and seconds
    public static final double kRamseteB = 1;
    public static final double kRamseteZeta = 0.3;

    // Booleans?
    public static final boolean kGyroReversed = true;
}
