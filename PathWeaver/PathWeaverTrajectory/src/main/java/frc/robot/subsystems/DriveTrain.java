package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class DriveTrain extends SubsystemBase
{    
    // Drive Train Talon Values / Location
    public static final int LEFT_FRONT = 1;
    public static final int LEFT_BACK = 2; //ENCODER
    public static final int RIGHT_FRONT = 4; //ENCODER
    public static final int RIGHT_BACK = 3;

    // Declare motor controllers
    private final WPI_TalonSRX leftFront = new WPI_TalonSRX(LEFT_FRONT);
    private final WPI_TalonSRX leftBack = new WPI_TalonSRX(LEFT_BACK);
    private final WPI_TalonSRX rightFront = new WPI_TalonSRX(RIGHT_FRONT);
    private final WPI_TalonSRX rightBack = new WPI_TalonSRX(RIGHT_BACK);

    // Motor Groups
    private final SpeedControllerGroup leftMotors = new SpeedControllerGroup(leftFront, leftBack);
    private final SpeedControllerGroup rightMotors = new SpeedControllerGroup(rightFront, rightBack);

    // Drive Train
    private final DifferentialDrive drive = new DifferentialDrive(leftMotors, rightMotors);

    // Gyro
    private final AHRS navx = new AHRS(SPI.Port.kMXP);

    // "Encoder"
    public final WPI_TalonSRX leftEncoder = leftFront;
    public final WPI_TalonSRX rightEncoder = rightBack;

    // Odometry class to track robot position
    private final DifferentialDriveOdometry odometry;

    public DriveTrain()
    {
        odometry = new DifferentialDriveOdometry(Rotation2d.fromDegrees(getHeading()));
        zeroHeading();
    }

    public Pose2d getPose()
    {
        return odometry.getPoseMeters();
    }

    public DifferentialDriveWheelSpeeds getWheelSpeeds()
    {
        return new DifferentialDriveWheelSpeeds(getLeftSpeed(), getRightSpeed());
    }

    public void resetOdometry(Pose2d pose)
    {
        resetEncoders();
        navx.reset();
        odometry.resetPosition(pose, Rotation2d.fromDegrees(getHeading()));
    }

    public void arcadeDrive(double fwd, double rot)
    {
        drive.arcadeDrive(fwd, rot);
    }

    public void tankDriveVolts(double leftVolts, double rightVolts)
    {
        leftMotors.setVoltage(leftVolts);
        rightMotors.setVoltage(rightVolts);
        drive.feed();
    }

    public void setMaxOutput(double maxOutput)
    {
        drive.setMaxOutput(maxOutput);
    }

    public void zeroHeading()
    {
        navx.reset();
    }

    public double getLeftDistance()
    {
        return leftEncoder.getSelectedSensorPosition(0);
    }

    public double getRightDistance()
    {
        return rightEncoder.getSelectedSensorPosition(0);
    }

    public double getLeftSpeed()
    {
        return leftEncoder.getSelectedSensorVelocity(0) / Constants.kGearRatio * 2 * Math.PI * Units.inchesToMeters(Constants.kWheelRadiusInches) / 60;
    }

    public double getRightSpeed()
    {
        return rightEncoder.getSelectedSensorVelocity() / Constants.kGearRatio * 2 * Math.PI * Units.inchesToMeters(Constants.kWheelRadiusInches) / 60;  
    }
    
    public void resetEncoders()
    {
        leftEncoder.setSelectedSensorPosition(0);
        rightEncoder.setSelectedSensorPosition(0);
    }

    public double getHeading()
    {
        return Math.IEEEremainder(navx.getAngle(), 360) * (Constants.kGyroReversed ? -1.0 : 1.0);
    }

    @Override
    public void periodic()
    {
        // Update the odometry in the periodic block
        odometry.update(Rotation2d.fromDegrees(getHeading()), getLeftDistance(), getRightDistance());
    }
}