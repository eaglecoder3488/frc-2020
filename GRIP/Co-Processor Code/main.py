# TO-DO:
#   - Clean up code
#   - Which color is which for (255,255,255)?
#   - CHECK IF THE DANG CODE IS EVEN IN THE RIGHT DIRECTION
#
# EXTRA NOTES:
#   - Camera is 640 x 480 (check pipeline)
#   - Thresholds: if mid is 320, left edge is 315, right edge is 325

from grip import GripPipeline
from networktables import NetworkTables
import cv2, numpy

USING_GUI = True

camera = "http://10.34.88.2:1181/stream.mjpg"
server = "10.34.88.2"

lineThickness = 2
decision = 0

def processData(pipeline):
    for contour in pipeline.filter_contours_output:
        netTable = NetworkTables.getTable('vision')
        x, y, w, h = cv2.boundingRect(contour)
        center = (w // 2) + x
        if center > 155 and center < 165:   
            decision = 0
            netTable.putNumber('decision', decision)
        if center <= 155:
            decision = -1
            netTable.putNumber('decision', decision)
        if center >= 165:
            decision = 1
            netTable.putNumber('decision', decision)

def main():
    print('Initializing...')
    NetworkTables.initialize(server=server)
    cap = cv2.VideoCapture(camera)
    pipeline = GripPipeline()
    
    print('Running Computer Vision. Check FRC Outline Viewer for Data!')

    while cap.isOpened():
        have_frame, frame = cap.read()

        if have_frame:
            pipeline.process(frame)
            processData(pipeline)
        
        if USING_GUI:
            cv2.drawContours(frame, pipeline.filter_contours_output, -1, (255,255,0), lineThickness)
            for contour in pipeline.filter_contours_output:
                x,y,w,h = cv2.boundingRect(contour)
                center = (w // 2) + x

                if center > 155 and center < 165:   
                    print("CENTERED")                     
                    cv2.line(frame, (center, 0), (center, 240), (0,255,0), lineThickness)
                if center <= 155:
                    print("MOVE LEFT")
                    cv2.line(frame, (center, 0), (center, 240), (0,0,255), lineThickness)
                if center >= 165:
                    print("MOVE RIGHT")
                    cv2.line(frame, (center, 0), (center, 240), (0,0,255), lineThickness)

                cv2.rectangle(frame,(x,y), (x+w,y+h), (255,0,0), lineThickness)
                
            cv2.imshow('COMPUTER VISION V1', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    
    print('Capture closed.')

if __name__ == '__main__':
    main()