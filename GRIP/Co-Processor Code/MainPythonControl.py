import requests
import numpy as np
import cv2 as cv

face_cascade = cv.CascadeClassifier('haarcascade_frontalface_default.xml')

previous_request = "KillAll"

count = 0

while True:
    img_res = requests.get("http://192.168.220.60:8080/shot.jpg")
    img_arr = np.array(bytearray(img_res.content), dtype = np.uint8)
    img = cv.imdecode(img_arr,-1)
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    
    for (x,y,w,h) in faces:
        cv.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        center = (w // 2) + x
        lineTrackerColor = (0, 0, 255)
        count = 0

        if center > 310:
            if previous_request != "TurnRight":
                requests.get("http://192.168.220.134/TurnRight")
                requests.get("http://192.168.220.134/MoveForward")
                previous_request = "TurnRight"
        elif center < 290:
            if previous_request != "TurnLeft":
                requests.get("http://192.168.220.134/TurnLeft")
                requests.get("http://192.168.220.134/MoveForward")
                previous_request = "TurnLeft"
        else:
            if previous_request != "NeutralDirection":
                requests.get("http://192.168.220.134/NeutralDirection")
                requests.get("http://192.168.220.134/MoveForward")
                previous_request = "NeutralDirection"
                lineTrackerColor = (0, 255, 0)
        
        cv.line(img, (center, 0), (center, 800), lineTrackerColor, 2)

        print(faces)
    
    count += 1
    if count >= 10:
        print("No face detected. Killing all.")
        requests.get("http://192.168.220.134/KillAll")
        count = 0
        previous_request = "KillAll"
    
    cv.imshow('Face Detection', img)
    
    if cv.waitKey(1) & 0xFF == ord('q'):
        break

cv.destroyAllWindows()