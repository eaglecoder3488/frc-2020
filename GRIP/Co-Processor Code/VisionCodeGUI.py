#!/usr/bin/python3

from grip import GripPipeline # Note: The contour filter is based off of a 30x30 square
from networktables import NetworkTables
import cv2
import numpy
import os

# Change booleans to enable or disable cetain functions (Note that corners will not be published if corresponding processing is False)
printValues = True
usingBoundingRect = True
usingMinAreaRect = True
publishingCornersMin = True

# Video device assignment:
videoDevice = 1

def publishData(pipeline):
    
    if usingBoundingRect:
        boundingRect_FULL = []
        center_x_positions = []
        center_y_positions = []
        widths = []
        heights = []

    if usingMinAreaRect:
        minAreaRect_FULL = []
        rect_center = []
        width_height = []
        angle_of_rotation = 0

    if publishingCornersMin:
        MIN_corner_a = []
        MIN_corner_b = []
        MIN_corner_c = []
        MIN_corner_d = []

    # Find the bounding rectangles of the contours to get variables for usage in robot code
    for contour in pipeline.filter_contours_output:
        
        if usingBoundingRect:
            boundingRect_FULL = cv2.boundingRect(contour) # Data mashed into one variable
            x, y, w, h = cv2.boundingRect(contour) # Data spread out over 4 variables

            # (Technically) Extra processing for boundingRect variables
            center_x_positions.append(x + w / 2)  # X and Y are coordinates of the top-left corner of the bounding box
            center_y_positions.append(y + h / 2)
            widths.append(w)
            heights.append(h)
        
        if usingMinAreaRect:
            minAreaRect_FULL = cv2.minAreaRect(contour) # Data mashed into one variable
            rect_center, width_height, angle_of_rotation = cv2.minAreaRect(contour) # Data spread out over 3 variables

            if publishingCornersMin:
                MIN_corner_a, MIN_corner_b, MIN_corner_c, MIN_corner_d = cv2.boxPoints(minAreaRect_FULL)

    # Publish to NetworkTables and print out data (if enabled):
    if usingBoundingRect:
        tableBound = NetworkTables.getTable('/vision/boundingRect')
        tableBound.putNumberArray('x', center_x_positions)
        tableBound.putNumberArray('y', center_y_positions)
        tableBound.putNumberArray('width', widths)
        tableBound.putNumberArray('height', heights)

        if printValues:
            print('---boundingRect Data---')
            print('boundingRect_FULL:')
            print(boundingRect_FULL)
            print('center_x_positions:')
            print(center_x_positions)
            print('center_y_positions:')
            print(center_y_positions)
            print('widths:')
            print(widths)
            print('heights:')
            print(heights)

            print('\n\n')
    
    if usingMinAreaRect:
        tableMin = NetworkTables.getTable('/vision/minAreaRect')
        tableMin.putNumberArray('rect_center', rect_center)
        tableMin.putNumberArray('width_height', width_height)
        tableMin.putNumber('angle_of_rotation', angle_of_rotation)

        if printValues:
            print('---minAreaRect Data---')
            print('minAreaRect_FULL:')
            print(minAreaRect_FULL)
            print('rect_center:')
            print(rect_center)
            print('width_height:')
            print(width_height)
            print('angle_of_rotation:')
            print(angle_of_rotation)

        if publishingCornersMin:
            tableMinCorners = NetworkTables.getTable('/vision/minAreaRect/corners')
            tableMinCorners.putNumberArray('MIN_corner_a', MIN_corner_a)
            tableMinCorners.putNumberArray('MIN_corner_b', MIN_corner_b)
            tableMinCorners.putNumberArray('MIN_corner_c', MIN_corner_c)
            tableMinCorners.putNumberArray('MIN_corner_d', MIN_corner_d)

            if printValues:
                print('MIN_corner_a:')
                print(MIN_corner_a)
                print('MIN_corner_b:')
                print(MIN_corner_b)
                print('MIN_corner_c:')
                print(MIN_corner_c)
                print('MIN_corner_d:')
                print(MIN_corner_d)

        if printValues:
            print('\n\n')


def main():
    print('Initializing NetworkTables...')
    NetworkTables.initialize(server='roborio-3488-frc.local')

    print('Creating Video Capture...')
    cap = cv2.VideoCapture(videoDevice)

    # Change Exposure (***change video device as needed***):
    os.system('v4l2-ctl -d /dev/video1 -c exposure_auto=1 -c exposure_absolute=0')

    print('Creating Pipeline...')
    pipeline = GripPipeline()
    
    print('Running Pipeline.')
    while cap.isOpened():
        have_frame, frame = cap.read()
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        if have_frame:
            pipeline.process(frame)
            publishData(pipeline)

        # Set thickness of lines
        thickness = 2
        
        # Normal contours:
        contourFrame = cv2.drawContours(frame, pipeline.filter_contours_output, -1, (0,255,0), thickness)
        
        # Min Bounding Rect:
        for contour in pipeline.filter_contours_output:
            minAreaRect_FULL = cv2.minAreaRect(contour)
            boxCorners = cv2.boxPoints(minAreaRect_FULL)
            
            # Numpy conversion
            ctr = numpy.array(boxCorners).reshape((-1,1,2)).astype(numpy.int32)

            contourFrame = cv2.drawContours(frame, [ctr], -1, (0,0,255), thickness)
        
        # Show all data in window
        cv2.imshow('FRC Vision', contourFrame)
    
    print('Capture closed.')


if __name__ == '__main__':
    main()
