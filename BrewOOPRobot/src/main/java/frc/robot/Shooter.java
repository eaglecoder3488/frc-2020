package frc.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

public class Shooter
{
    public static final int SHOOTER_CAN = 8;
    public static final double SPEED_OUT = 0.65;

    private WPI_TalonSRX shooter;

    public Shooter()
    {
        shooter = new WPI_TalonSRX( Shooter.SHOOTER_CAN );
    }

    public void shoot()
    {
        shooter.set( -Shooter.SPEED_OUT );
    }

    public void stop()
    {
        shooter.set( 0.0 );
    }
}