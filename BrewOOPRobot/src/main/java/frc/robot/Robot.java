package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;

public class Robot extends TimedRobot 
{
  private Controller xBoxPad;
  private DriveTrain driveTrain;
  private Intake intake;
  private Conveyor conveyor;
  private Indexer indexer;
  private Shooter shooter;

  @Override
  public void robotInit() 
  {
    if ( xBoxPad == null )  xBoxPad = new Controller();
    if ( driveTrain == null )  driveTrain = new DriveTrain();
    if ( intake == null )  intake = new Intake();
    if ( conveyor == null )  conveyor = new Conveyor();
    if ( indexer == null )  indexer = new Indexer();
    if ( shooter == null )  shooter = new Shooter();
  }
 
  @Override
  public void teleopInit() 
  {
    indexer.makeEmpty();
  }

  
  @Override
  public void teleopPeriodic() 
  {
    double leftStickX = xBoxPad.getLeftStickX( true );
    double leftStickY = xBoxPad.getLeftStickY( true );

    driveTrain.arcadeDrive( leftStickX, leftStickY );

    // TAKING IN BALLS
    if ( xBoxPad.getRightTrigger( true ) > 0.0 )
    {
      intake.intakeBalls();
      conveyor.intakeBalls();
    }
    else
    {
      intake.stop();
      conveyor.stop();
    }
    
    // STOP THE CONVEYOR IF THE BALL IS AT THE INDEXER SO IT DOESNT SUCK
    if ( indexer.isBallAtIn() )
      conveyor.stop();

    // INDEX THE BALLS
    indexer.indexBalls();

    // SHOOTING
    if ( xBoxPad.getXButton() )
      shooter.shoot();
    else
      shooter.stop();

    // OVERRIDES GO HERE SO THAT THEY CANCEL OUT PREVIOUS COMMANDS
  }
}
