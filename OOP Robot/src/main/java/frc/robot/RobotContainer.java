package frc.robot;

import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.controller.RamseteController;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj.trajectory.TrajectoryUtil;

public class RobotContainer
{
    private autoDriveTrain AutoDriveTrain;
    private Indexer indexer;
    private Shooter shooter;
    private Counter counter;
    private Intake intake;
    private Conveyor conveyor;

    public static final String ROBOT_COLOR = "RED"; // RED OR BLUE, ALL CAPS
    public static final String ROBOT_POS = "TOP"; // TOP, MID, OR BOTTOM, BOTTOM BEING CLOSEST TO STANDS, ALL CAPS
    public static final Boolean USING_INIT = true;
    public static final Boolean USING_TRIO = true;
    public static final Boolean USING_DUO = false;

    // Holders
    String initPath;
    String trioInPath;
    String trioOutPath;
    String duoInPath;
    String duoOutPath;

    public RobotContainer()
    {
        AutoDriveTrain = new autoDriveTrain();
    }

    public void resetGyro()
    {
        AutoDriveTrain.resetGyro();
    }

    public void initializeTrajectories()
    {
        // INIT PATHS
        String redTopInitJSON = "PathWeaver/output/redTopInit.wpilib.json";
        String redMidInitJSON = "PathWeaver/output/redMidInit.wpilib.json";
        String redBottomInitJSON = "PathWeaver/output/redBottomInit.wpilib.json";
        String blueTopInitJSON = "PathWeaver/output/blueTopInit.wpilib.json";
        String blueMidInitJSON = "PathWeaver/output/blueMidInit.wpilib.json";
        String blueBottomInitJSON = "PathWeaver/output/blueBottomInit.wpilib.json";

        // TRIO PATHS
        String redTrioJSON = "PathWeaver/output/redTrio.wpilib.json";
        String blueTrioJSON = "PathWeaver/output/blueTrio.wpilib.json";
        String redShootTrioJSON = "PathWeaver/output/redShootTrio.wpilib.json";
        String blueShootTrioJSON = "PathWeaver/output/blueShootTrio.wpilib.json";

        // DUO PATHS
        String redDuoJSON = "PathWeaver/output/redSpinnerDuo.wpilib.json";
        String blueDuoJSON = "PathWeaver/output/blueSpinnerDuo.wpilib.json";
        String redShootDuoJSON = "PathWeaver/output/redShootDuo.wpilib.json";
        String blueShootDuoJSON = "PathWeaver/output/blueShootDuo.wpilib.json";

        if(ROBOT_COLOR == "RED")
        {
            if(USING_INIT)
            {
                if(ROBOT_POS == "TOP") initPath = redTopInitJSON;
                if(ROBOT_POS == "MID") initPath = redMidInitJSON;
                if(ROBOT_POS == "BOTTOM") initPath = redBottomInitJSON;
            }

            if(USING_TRIO)
            {
                trioInPath = redTrioJSON;
                trioOutPath = redShootTrioJSON;
            }

            if(USING_DUO)
            {
                duoInPath = redDuoJSON;
                duoOutPath = redShootDuoJSON;
            }
        }

        if(ROBOT_COLOR == "BLUE")
        {
            if(USING_INIT)
            {
                if(ROBOT_POS == "TOP") initPath = blueTopInitJSON;
                if(ROBOT_POS == "MID") initPath = blueMidInitJSON;
                if(ROBOT_POS == "BOTTOM") initPath = blueBottomInitJSON;
            }

            if(USING_TRIO)
            {
                trioInPath = blueTrioJSON;
                trioOutPath = blueShootTrioJSON;
            }

            if(USING_DUO)
            {
                duoInPath = blueDuoJSON;
                duoOutPath = blueShootDuoJSON;
            }
        }
    }
    
    private Trajectory initTrajectory()
    {
        TrajectoryConfig config = new TrajectoryConfig( Units.feetToMeters( 6 ), Units.feetToMeters( 4 ) );
        config.setKinematics( AutoDriveTrain.getKinematics() );
        try
        {
            Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve(initPath);
            Trajectory trajectory = TrajectoryUtil.fromPathweaverJson(trajectoryPath);
            return trajectory;
        }
        catch(IOException ex)
        {
            DriverStation.reportError("Init trajectory not opened: " + initPath, ex.getStackTrace());
        }
        return null;
    }

    private Trajectory trioInTrajectory()
    {
        TrajectoryConfig config = new TrajectoryConfig( Units.feetToMeters( 6 ), Units.feetToMeters( 4 ) );
        config.setKinematics( AutoDriveTrain.getKinematics() );
        try
        {
            Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve(trioInPath);
            Trajectory trajectory = TrajectoryUtil.fromPathweaverJson(trajectoryPath);
            return trajectory;
        }
        catch(IOException ex)
        {
            DriverStation.reportError("trioIn trajectory not opened: " + trioInPath, ex.getStackTrace());
        }
        return null;
    }

    public Command followTrajectories()
    {
        AutoDriveTrain.resetOdometry( new Pose2d() );

        Trajectory trajectory = initTrajectory();

        RamseteCommand command = new RamseteCommand(
            trajectory,
            AutoDriveTrain::getPose,
            new RamseteController( autoDriveTrain.kRamseteB, autoDriveTrain.kRamseteZeta ),
            AutoDriveTrain.getSimpleMotorFeedForward(),
            AutoDriveTrain.getKinematics(),
            AutoDriveTrain::getSpeeds,
            AutoDriveTrain.getLeftPIDCOntroller(),
            AutoDriveTrain.getRightPIDCOntroller(),
            AutoDriveTrain::tankDriveVolts,
            AutoDriveTrain
        );
        return command.andThen(shootBall());
    }

    public Command shootBall()
    {
        while(!shootBall().isFinished())
        {
            shooter.shoot();
            counter.increment();
            if(counter.isShooterReady()) // Shooter warmup is over
            {
                indexer.advanceBalls();
            if(indexer.isBallAtOut()) // "Neutralize" counter if ball isn't over sensor
                counter.decrement();
            else
                counter.increment();
        
            if(counter.isIndexerFinished())
                counter.reset();
            }
        }
        return shootBall().withTimeout(5).andThen(followTrioIn());
    }

    public Command followTrioIn()
    {
        AutoDriveTrain.resetOdometry( new Pose2d(new Translation2d(0, 0), new Rotation2d(180)) );

        Trajectory trajectory = trioInTrajectory();

        RamseteCommand command = new RamseteCommand(
            trajectory,
            AutoDriveTrain::getPose,
            new RamseteController( autoDriveTrain.kRamseteB, autoDriveTrain.kRamseteZeta ),
            AutoDriveTrain.getSimpleMotorFeedForward(),
            AutoDriveTrain.getKinematics(),
            AutoDriveTrain::getSpeeds,
            AutoDriveTrain.getLeftPIDCOntroller(),
            AutoDriveTrain.getRightPIDCOntroller(),
            AutoDriveTrain::tankDriveVolts,
            AutoDriveTrain
        );
        return command.andThen(() -> AutoDriveTrain.tankDriveVolts(0, 0));
    }

    // Didn't want to delete this
    private void holder()
    {
        TrajectoryConfig config = new TrajectoryConfig( Units.feetToMeters( 6 ), Units.feetToMeters( 4 ) );
        Trajectory tempTrajectory = TrajectoryGenerator.generateTrajectory(
            Arrays.asList( new Pose2d(), new Pose2d( 5, 0, new Rotation2d() ) ),

            config
        );

        Trajectory trajectory = TrajectoryGenerator.generateTrajectory(
        // Start at the origin facing the +X direction
        new Pose2d(0, 0, new Rotation2d(0)),
        // Pass through these two interior waypoints, making an 's' curve path
        List.of(
            new Translation2d(1, 1),
            new Translation2d(2, -1)
        ),
        // End 3 meters straight ahead of where we started, facing forward
        new Pose2d(3, 0, new Rotation2d(0)),
        // Pass config
        config
        );
    }
}