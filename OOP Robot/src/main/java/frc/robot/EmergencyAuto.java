package frc.robot;

public class EmergencyAuto
{
    private DriveTrain driveTrain;
    private Counter counter;
    private Boolean finishedDriving;
    private Indexer indexer;
    private Shooter shooter;

    public EmergencyAuto()
    {
        if(driveTrain == null) driveTrain = new DriveTrain();
        if(indexer == null) indexer = new Indexer();
        if(shooter == null) shooter = new Shooter();

        finishedDriving = false;
    }

    public void run()
    {
        if(counter.getValue() < 25 && !finishedDriving)
        {
            driveTrain.arcadeDrive(.7, 0);
        }
        else
        {
            finishedDriving = true;
            counter.reset();
        }
        if(finishedDriving)
        {
            shooter.shoot();
            counter.increment();
            if(counter.isShooterReady()) // Shooter warmup is over
            {
                indexer.advanceBalls();
                if(indexer.isBallAtOut()) // "Neutralize" counter if ball isn't over sensor
                    counter.decrement();
                else
                    counter.increment();
        
                if(counter.isIndexerFinished())
                {
                    counter.reset();
                    indexer.stop();
                }
            }
        }
    }
}