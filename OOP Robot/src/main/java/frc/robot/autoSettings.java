package frc.robot;

public class autoSettings
{
    public static final String ROBOT_COLOR = "RED"; // RED OR BLUE, ALL CAPS
    public static final String ROBOT_POS = "TOP"; // TOP, MID, OR BOTTOM, BOTTOM BEING CLOSEST TO STANDS, ALL CAPS
    public static final Boolean USING_INIT = true;
    public static final Boolean USING_TRIO = false;
    public static final Boolean USING_DUO = false;
}