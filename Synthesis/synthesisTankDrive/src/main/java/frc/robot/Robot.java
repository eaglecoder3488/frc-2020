package frc.robot;

import edu.wpi.first.wpilibj.PWMVictorSPX;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

public class Robot extends TimedRobot
{
  public static final boolean USING_SYNTHESIS = true;

  public static final int FRONT_LEFT = 0;
  public static final int FRONT_RIGHT = 1;
  public static final int BACK_LEFT = 2;
  public static final int BACK_RIGHT = 3;

  public static final int CONTROLLER_PORT = 0;
  
  XboxController xboxPad;

  PWMVictorSPX leftFront;
  PWMVictorSPX leftBack;
  PWMVictorSPX rightFront;
  PWMVictorSPX rightBack;

  SpeedControllerGroup leftGroup;
  SpeedControllerGroup rightGroup;

  DifferentialDrive driveTrain;

  @Override
  public void robotInit()
  {
    if(driveTrain == null)
    {
      leftFront = new PWMVictorSPX(FRONT_LEFT);
      leftBack = new PWMVictorSPX(BACK_LEFT);
      rightFront = new PWMVictorSPX(FRONT_RIGHT);
      rightBack = new PWMVictorSPX(BACK_RIGHT);

      leftGroup = new SpeedControllerGroup(leftFront, leftBack);
      rightGroup = new SpeedControllerGroup(rightFront, rightBack);

      driveTrain = new DifferentialDrive(leftGroup, rightGroup);
    }

    if(xboxPad == null)
    {
      xboxPad = new XboxController(CONTROLLER_PORT);
    }
  }

  @Override
  public void teleopPeriodic()
  {
    /*  Axis Information
    Axis 0: LJoyCon -> Left = -1, Right = 1
    Axis 1: LJoyCon -> Up = -1, Down = 1
    Axis 3: RJoyCon -> Left = -1, Right = 1
    Axis 4: RJoyCon -> Up = -1, Down = 1  */
    driveTrain.tankDrive(xboxPad.getRawAxis(1), xboxPad.getRawAxis(4));
  }

  @Override
  public void robotPeriodic()
  {

  }
  @Override
  public void autonomousInit()
  {

  }

  @Override
  public void autonomousPeriodic()
  {

  }

  @Override
  public void testPeriodic()
  {

  }
}
