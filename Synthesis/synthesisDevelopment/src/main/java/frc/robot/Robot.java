package frc.robot;

import edu.wpi.first.wpilibj.PWMVictorSPX;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.drive.MecanumDrive;

public class Robot extends IterativeRobot
{
  public static final boolean USING_SYNTHESIS = true;

  public static final int LEFT_FRONT = 0;
  public static final int RIGHT_FRONT = 1;
  public static final int LEFT_BACK = 2;
  public static final int RIGHT_BACK = 3;
  
  public static final int CONTROLLER_PORT = 0;

  XboxController xboxPad;

  PWMVictorSPX leftFront;
  PWMVictorSPX rightFront;
  PWMVictorSPX leftBack;
  PWMVictorSPX rightBack;

  MecanumDrive driveTrain;

  @Override
  public void robotInit()
  {
    if(xboxPad == null)
    {
      xboxPad = new XboxController(CONTROLLER_PORT);
    }

    if(driveTrain == null)
    {
      leftFront = new PWMVictorSPX(LEFT_FRONT);
      rightFront = new PWMVictorSPX(RIGHT_FRONT);
      leftBack = new PWMVictorSPX(LEFT_BACK);
      rightBack = new PWMVictorSPX(RIGHT_BACK);

      driveTrain = new MecanumDrive(leftFront, leftBack, rightFront, rightBack);
    }
  }

  @Override
  public void teleopPeriodic()
  { 
    /*  Axis Information
    Axis 0: LJoyCon -> Left = -1, Right = 1
    Axis 1: LJoyCon -> Up = -1, Down = 1
    Axis 3: RJoyCon -> Left = -1, Right = 1
    Axis 4: RJoyCon -> Up = -1, Down = 1  */
    if (USING_SYNTHESIS)
    {
      driveTrain.driveCartesian(xboxPad.getRawAxis(0), xboxPad.getRawAxis(1), xboxPad.getRawAxis(3));
    }
    else
    {
      double x = xboxPad.getRawAxis(0);
      double y = xboxPad.getRawAxis(1);
      double z = xboxPad.getRawAxis(3);
      driveTrain.driveCartesian(x, y, z);
    }
  }

  @Override
  public void robotPeriodic()  { }

  @Override
  public void autonomousInit()  { }

  @Override
  public void autonomousPeriodic()  { }

  @Override
  public void testPeriodic()  { }
}
